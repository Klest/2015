﻿<%@include file="WEB-INF/includes/header.jsp" %>


<div class="catogs row">
<div class="col-md-4 col-sm-6" style="padding:0">
    <div style="padding:5px">
        <a href="/products?action=inSubcategory&id=${homePageWoodCarving.id}" class="ui image">
            <div class="ui dimmer dimmers dimfirst">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageWoodCarving.title}</h1>
                  </div>
                </div>
              </div>

              <div class="ui dimmer dimmers active visible-xs visible-sm">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageWoodCarving.title}</h1>
                  </div>
                </div>
              </div>

            <img src="/assets/img/products/${homePageWoodCarving.picture}">
        </a>
    </div>

    <div style="padding:5px">
        <a href="/products?action=inSubcategory&id=${homePageHouseWarming.id}" class="ui image">
            <div class="ui dimmer dimmers dimfirst">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageHouseWarming.title}</h1>
                  </div>
                </div>
              </div>

              <div class="ui dimmer dimmers visible-xs visible-sm active">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageHouseWarming.title}</h1>
                  </div>
                </div>
              </div>
            <img src="/assets/img/products/${homePageHouseWarming.picture}">
        </a>
    </div>
</div>

<div class="col-md-4 col-sm-6 col-md-push-4" style="padding:5px; margin-top:1px;">
<a href="/products?action=inSubcategory&id=${homePageBijuteria.id}" class="ui image">
    <div class="ui dimmer dimmers dimfirst">
        <div class="content">
          <div class="center">
            <h1 class="ui inverted header">${homePageBijuteria.title}</h1>
          </div>
        </div>
      </div>

      <div class="ui dimmer dimmers visible-xs visible-sm active">
        <div class="content">
          <div class="center">
            <h1 class="ui inverted header">${homePageBijuteria.title}</h1>
          </div>
        </div>
      </div>

    <img src="/assets/img/products/${homePageBijuteria.picture}">
</a><br>
</div>

<div class="col-md-4 col-md-pull-4" style="padding:0">
    
    <div class="col-sm-6 col-md-12" style="padding:5px">
        <a href="/products?action=inSubcategory&id=${homePagePainting.id}" class="ui image">
            <div class="ui dimmer dimmers dimfirst">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePagePainting.title}</h1>
                  </div>
                </div>
              </div>

              <div class="ui dimmer dimmers visible-xs visible-sm active">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePagePainting.title}</h1>
                  </div>
                </div>
              </div>
            <img src="/assets/img/products/${homePagePainting.picture}">
        </a>
    </div>


    <div class="col-sm-6 col-md-12" style="padding:5px">
        <a href="/products?action=inSubcategory&id=${homePageAnniversary.id}" class="ui image">
            <div class="ui dimmer dimmers dimfirst">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageAnniversary.title}</h1>
                  </div>
                </div>
              </div>

              <div class="ui dimmer dimmers visible-xs visible-sm active">
                <div class="content">
                  <div class="center">
                    <h1 class="ui inverted header">${homePageAnniversary.title}</h1>
                  </div>
                </div>
              </div>
            <img src="/assets/img/products/${homePageAnniversary.picture}">
        </a>
    </div>
</div>

</div>
<script>
$('.dimfirst')
  .dimmer({
    on: 'hover'
  })
;
</script>

<script src=" https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>

<script src="/assets/js/theapp.js"></script>

<div>
<h1 class="ui horizontal divider header" style="margin:20px 0;font-family: copyFont;">
  <i class="heart icon" style="color: #713364"></i>
  <fmt:message key="plainText.popularNow"/>
</h1><%--
May be use separete file and include here and in /products/index.jsp
<jsp:include page="___PATH_TO_YOUR_FILE___"/> better as a path use /WEB-INF/view/
--%>
<div id="myPro" style="display:none">
<c:forEach items="${requestScope.products}" var="product">
    
        <div class="myPros">
            <a href="/products?action=find&product=${product.id}"> ${product.title} </a>
            <img width="200px" style="border-radius: 50%" src="/assets/img/products/${product.mainPicture}"/>
            <div> ${product.description} </div>
            <div><fmt:message key="form.price"/> : ${product.price} AZN</div>
        </div>
    <hr>
</c:forEach>
</div>
<div>
    <div products-list></div>
</div>

</div>
<%@include file="WEB-INF/includes/footer.jsp" %>