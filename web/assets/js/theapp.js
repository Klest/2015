var app = angular.module('seymur', []);

app.directive('productsList', function () {
    return {
        templateUrl: "/assets/html/productsList.html",
        controller: 'productsController'
    }
});
app.directive('theItem', function () {
    return {
        templateUrl: "/assets/html/item.html"
    }
});

app.controller("productsController", function ($scope) {
    var aArray = document.getElementsByClassName("myPros");
    var items = new Object();
    var twoRowf = new Object();
    var twoRows = new Object();
    var thRowf = new Object();
    var thRows = new Object();
    var thRowt = new Object();
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    for (var i = 0; i < aArray.length; i++) {
        var item = {
            name: aArray[i].children[0].innerText,
            url: aArray[i].children[0].href,
            image: aArray[i].children[1].src,
            melumat: aArray[i].children[2].innerText,
            qiymet: aArray[i].children[3].innerText
        }
        items[i] = item;

        if(i%3==0)
            thRowf[Object.size(thRowf)] = item;
        else if(i%3==1)
            thRows[Object.size(thRows)] = item;
        else thRowt[Object.size(thRowt)] = item;

        if (i % 2 != 1)
            twoRowf[Object.size(twoRowf)] = item;
        else
            twoRows[Object.size(twoRowf)] = item;
    }
    var element = document.getElementById("myPro");
    element.parentNode.removeChild(element);
    $scope.oneRow = items;
    $scope.twoRowf = twoRowf;
    $scope.twoRows = twoRows;
    $scope.thRowf = thRowf;
    $scope.thRows = thRows;
    $scope.thRowt = thRowt;
    console.log(thRowf,thRows,thRowt);
})