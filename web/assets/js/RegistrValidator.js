function check(field, value) {
    console.log(field + " " + value);
    var ajaxRequest = new XMLHttpRequest;
    ajaxRequest.open('POST', '/signup', true);
    ajaxRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ajaxRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajaxRequest.onreadystatechange = function () {
        if (this.status === 200)
            if (this.readyState == 4)
                if (ajaxRequest.responseText === "true") // If response text is "true" then field is valid
                    document.getElementById(field + "_msg").innerHTML = "ok";
                else // Otherwise response text contains an error message
                    document.getElementById(field + "_msg").innerHTML = ajaxRequest.responseText;
    };
    ajaxRequest.send("validate=" + field + "&" + field + "=" + encodeURIComponent(value));
    return false;
}

function checkPassword2(){
    if(regForm.password.value==regForm.password2.value) password2_msg.style.display = "none";// ok
    else password2_msg.style.display = "block";// passwords don't match
}