﻿<%@ include file="/WEB-INF/includes/header.jsp" %>
<form id="regForm" method="POST" action="<c:url value="/login"/>">
    <h2 class="ui horizontal divider header">
      <i class="user icon" style="color: #713364"></i>
      Enter to your account
    </h2>
    <table class="ui very basic table">
        <tr>
            <td><fmt:message key="form.login"/></td>
            <td><input name="login"  class="form-control"></td>
        </tr>
        <tr>
            <td><fmt:message key="form.password"/></td>
            <td><input type="password" name="password" class="form-control"></td>
        </tr>
        <tr>
            <td><fmt:message key="form.submit" var="submit"/></td>
            <td ><input type="submit" value="${submit}" class="registerbutton"></td>
        </tr>
    </table>
</form>
<%@ include file="/WEB-INF/includes/footer.jsp" %>