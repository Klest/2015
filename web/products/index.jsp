<%@ include file="/WEB-INF/includes/header.jsp" %>
<script src=" https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>

<script src="/assets/js/theapp.js"></script>
<%-- Product Filtering. May be put it to right/left side of the page --%>
<form method="post" action>
    <div class="row">
        <div class="col-md-4"><fmt:message key="plainText.sortingBy"/>: <select name="sorting" class="form-control">
            <option value="1" <c:if test="${sessionScope.sorting.sortingCode == 1}">selected</c:if>>
                <fmt:message key="plainText.sorting.price"/> &#709;
            </option>
            <option value="2" <c:if test="${sessionScope.sorting.sortingCode == 2}">selected</c:if>>
                <fmt:message key="plainText.sorting.price"/> &#708;
            </option>
            <option value="3" <c:if test="${sessionScope.sorting.sortingCode == 3}">selected</c:if>>
                <fmt:message key="plainText.sorting.alphabeticaly"/> &#709;
            </option>
            <option value="4" <c:if test="${sessionScope.sorting.sortingCode == 4}">selected</c:if>>
                <fmt:message key="plainText.sorting.alphabeticaly"/> &#708;
            </option>
            <option value="5" <c:if test="${sessionScope.sorting.sortingCode == 5}">selected</c:if>>
                <fmt:message key="plainText.sorting.popularity"/>
            </option>
            <option value="6" <c:if test="${sessionScope.sorting.sortingCode == 6}">selected</c:if>>
                <fmt:message key="plainText.sorting.new"/>
            </option>
        </select></div>
        <div class="col-md-4"><fmt:message key="plainText.pageSize"/>: <input type="number" class="form-control" name="pageSize" value="${sessionScope.paging.pageSize}"></div>
    </div>
    <fmt:message key="form.submit" var="submit"/>
    <input type="submit" class="registerbutton" style="margin-top:20px;" value="${submit}">
</form>
<%-- Filtering end --%>
<hr>
<%-- List of selected products --%>
<c:if test="${empty requestScope.products}">
    (<fmt:message key="plainText.empty"/>)
</c:if>
<c:if test="${not empty requestScope.products}">
    <!--Start with screen optimizing -->

    <div products-list></div>

    <!--End with screen optimizing -->

    <div id="myPro" style="display:none">
    <c:forEach items="${requestScope.products}" var="product">

        <div class="myPros">
            <a href="/products?action=find&product=${product.id}"> ${product.title} </a>
            <img width="200px" style="border-radius: 50%" src="/assets/img/products/${product.mainPicture}"/>
            <div> ${product.description} </div>
            <div><fmt:message key="form.price"/> : ${product.price} AZN</div>
        </div>
    </c:forEach>
        <%--
        <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
Provide these infos and then write an email

Product can belong to many categories and also you can select products from many categories,
But if category or subcategory is chosen then:--%><!--
        <c:if test="${not empty requestScope.category}">
            <div>${requestScope.category.title}</div>
            <div>${requestScope.category.id}</div>
        </c:if>
        </div>!--><%--
            <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



                <div class="col-md-4 col-sm-6 myPros" style="margin-bottom:10px;" >

                        <div class="ui segment" style="background-color: #713364; color: #fff;">
                            <a href="/products?action=find&id=${product.id}" style="color:#fff">
                                ${product.title}
                            </a>
                        </div>


                    <div style="margin-bottom:10px;">
                        <div class="ui fluid image">
                            <div class="ui purple ribbon label" style="font-size:inherit;">
                                    Category title
                            </div>
                            <img width="200px" src="/assets/img/products/${product.id}.${product.picturesExtension}"/>
                        </div>
                    </div>

                    <div>
                            ${product.description}
                    </div>



                    <div class="text-right"style="padding:10px;">

                            <a class="ui tag label" style="font-size:inherit;">
                                <fmt:message key="form.price"/> : ${product.price} AZN
                            </a>
                    </div>

                </div>


            <table border="1px" width="600px">
                <tr>
                    <td><fmt:message key="form.title"/></td>
                    <td>${product.title}</td>
                </tr>
                <tr>
                    <td><fmt:message key="form.description"/></td>
                    <td>${product.description}</td>
                </tr>
                <tr>
                    <td>
                        <img width="200px" src="/assets/img/products/${product.id}.${product.picturesExtension}"/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="form.price"/></td>
                    <td><fmt:formatNumber value="${product.price}" maxFractionDigits="2" minFractionDigits="2"/></td>
                </tr>
                <c:if test="${requestScope.isLogged}">
                    <tr>
                        <td><a href="/cart?action=add&id=${product.id}"><fmt:message key="action.add.toCart"/></a></td>
                    </tr>
                </c:if>
                <c:if test="${requestScope.isAdmin}">
                    <tr>
                        <td><a href="/admin/product?id=${product.id}"><fmt:message key="action.change"/></a></td>
                    </tr>
                </c:if>
            </table>
            <hr>
            --%>
    
    </div>
</c:if>
<%@include file="/WEB-INF/includes/footer.jsp" %>