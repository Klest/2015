<%@include file="/WEB-INF/includes/header.jsp" %>
<c:forEach items="${requestScope.completedOrders}" var="cart">
    <table border="1">
        <tr>
            <td><fmt:message key="plainText.creationTime"/></td>
            <td>${cart.creationTime}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.price"/></td>
            <td><fmt:formatNumber value="${cart.price}" minFractionDigits="2" maxFractionDigits="2"/></td>
        </tr>
        <c:forEach items="${cart.products}" var="entry">
            <tr>
                <td>${entry.key.title}</td>
                <td>${entry.value}</td>
            </tr>
        </c:forEach>
    </table>
    <br>
</c:forEach>
<%@include file="/WEB-INF/includes/footer.jsp" %>