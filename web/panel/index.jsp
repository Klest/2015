<%@ include file="/WEB-INF/includes/header.jsp" %>
<c:if test="${not empty requestScope.form}">
    <jsp:include page="/WEB-INF/includes/forms/${requestScope.form}.jsp"/>
</c:if>
<c:if test="${empty requestScope.form}">
    <fmt:message key="action.set" var="set"/>
    <fmt:message key="action.edit" var="edit"/>

    <table>
        <tr>
            <td><fmt:message key="form.password"/></td>
            <td>${sessionScope.user.covertlyPassword} <a href="?change=password">${edit}</a></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td>
                <c:if test="${not empty sessionScope.user.attributes.email}">
                    ${sessionScope.user.attributes.email}
                </c:if>
                <a href="?change=email">
                    <c:if test="${not empty sessionScope.user.attributes.email}">
                        ${edit}
                    </c:if>
                    <c:if test="${empty sessionScope.user.attributes.email}">
                        ${set}
                    </c:if>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <fmt:message key="form.name"/>
            </td>
            <td>
                <c:if test="${not empty sessionScope.user.attributes.name}">
                    ${sessionScope.user.attributes.name}
                </c:if>
                <a href="?change=name">
                    <c:if test="${not empty sessionScope.user.attributes.name}">
                        ${edit}
                    </c:if>
                    <c:if test="${empty sessionScope.user.attributes.name}">
                        ${set}
                    </c:if>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <fmt:message key="form.surname"/>
            </td>
            <td>
                <c:if test="${not empty sessionScope.user.attributes.surname}">
                    ${sessionScope.user.attributes.surname}
                </c:if>
                <a href="?change=surname">
                    <c:if test="${not empty sessionScope.user.attributes.surname}">
                        ${edit}
                    </c:if>
                    <c:if test="${empty sessionScope.user.attributes.surname}">
                        ${set}
                    </c:if>
                </a>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="form.mobile"/></td>
            <td>
                <c:if test="${not empty sessionScope.user.attributes.mobile}">
                    ${sessionScope.user.attributes.mobile}
                </c:if>
                <a href="?change=mobile">
                    <c:if test="${empty sessionScope.user.attributes.mobile}">
                        ${set}
                    </c:if>
                    <c:if test="${not empty sessionScope.user.attributes.mobile}">
                        ${edit}
                    </c:if>
                </a>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="form.address"/></td>
            <td>
                <c:if test="${not empty sessionScope.user.attributes.address}">
                    ${sessionScope.user.attributes.address}
                </c:if>
                <a href="?change=address">
                    <c:if test="${empty sessionScope.user.attributes.address}">
                        ${set}
                    </c:if>
                    <c:if test="${not empty sessionScope.user.attributes.address}">
                        ${edit}
                    </c:if>
                </a>
            </td>
        </tr>
    </table>
</c:if>
<%@ include file="/WEB-INF/includes/footer.jsp" %>