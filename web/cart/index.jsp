<%@include file="/WEB-INF/includes/header.jsp" %>
<c:if test="${requestScope.cartIsEmpty}">
    (<fmt:message key="plainText.empty"/>)
</c:if>
<c:if test="${!requestScope.cartIsEmpty}">
    <table class="ui very basic table">
        <c:forEach items="${sessionScope.cart.products}" var="entry">
            <tr>
                <td><fmt:message key="plainText.product"/></td>
                <td><a href="/products?action=find&product=${entry.key.id}">${entry.key.title}</a></td>
            </tr>
            <tr>
                <td><fmt:message key="plainText.amount"/></td>
                <td>${entry.value}</td>
            </tr>
            <tr>
                <td colspan="2"><a href="?action=remove&id=${entry.key.id}" class="ui labeled icon red basic button" style="font-size:inherit;">
                    <i class="trash icon"></i>
                    <fmt:message key="action.remove"/>
                </a>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td><fmt:message key="form.price"/></td>
            <td><fmt:formatNumber value="${sessionScope.cart.price}" maxFractionDigits="2" minFractionDigits="2"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <a href="/cart?action=confirm" class="ui labeled icon purple  button" style="font-size:inherit">
                    <i class="cart icon"></i>
                    <fmt:message key="action.buy"/>
                </a>
            </td>
        </tr>
    </table>
</c:if>
<%@include file="/WEB-INF/includes/footer.jsp" %>