<%@ include file="/WEB-INF/includes/header.jsp" %>
<form method="post" enctype="multipart/form-data">
    <h3><fmt:message key="form.name"/> (AZ)</h3>
    <input name="title_az" value="${requestScope.subcategory.titleAz}">

    <h3><fmt:message key="form.name"/> (RU)</h3>
    <input name="title_ru" value="${requestScope.subcategory.titleRu}">

    <h3><fmt:message key="form.name"/> (EN)</h3>
    <input name="title_en" value="${requestScope.subcategory.titleEn}">

    <h3><fmt:message key="form.category"/></h3>
    <select name="category">
        <c:forEach items="${requestScope.categories}" var="category">
            <option
                    <c:if test="${category.id==requestScope.subcategory.categoryId}">selected</c:if> value="${category.id}">${category.title}</option>
        </c:forEach>
    </select>

    <h3><fmt:message key="form.image"/></h3>
    <c:if test="${not empty requestScope.subcategory}">
        <img width="300px" src="/assets/img/products/${requestScope.subcategory.picture}"/><br>
    </c:if>
    <c:if test="${empty requestScope.subcategory}">
        <input type="file" name="picture" accept="image/jpeg,image/png,image/gif"><br>
    </c:if>
    <fmt:message key="action.save" var="submit"/>
    <input type="submit" value="${submit}">
</form>
<%@ include file="/WEB-INF/includes/footer.jsp" %>