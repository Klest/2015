<%@ include file="/WEB-INF/includes/header.jsp" %>
<form method="post" enctype="multipart/form-data">
    <h3><fmt:message key="form.name"/> (AZ)</h3>
    <input name="title_az" value="${requestScope.product.titleAz}">

    <h3><fmt:message key="form.name"/> (RU)</h3>
    <input name="title_ru" value="${requestScope.product.titleRu}">

    <h3><fmt:message key="form.name"/> (EN)</h3>
    <input name="title_en" value="${requestScope.product.titleEn}">

    <h3><fmt:message key="form.description"/> (AZ)</h3>
    <textarea name="description_az">${requestScope.product.descriptionAz}</textarea>

    <h3><fmt:message key="form.description"/> (RU)</h3>
    <textarea name="description_ru">${requestScope.product.descriptionRu}</textarea>

    <h3><fmt:message key="form.description"/> (EN)</h3>
    <textarea name="description_en">${requestScope.product.descriptionEn}</textarea>

    <h3><fmt:message key="form.subcategories"/></h3> <%-- Going to be long --%>
    <c:forEach items="${requestScope.subcategories}" var="subcategory">
        <input type="checkbox" name="subcategory_${subcategory.id}" value="true"
               <c:if test="${requestScope.checkedSubcategories.contains(subcategory)}">checked</c:if> > ${subcategory.title}<br>
    </c:forEach>

    <h3><fmt:message key="form.image"/></h3>
    <c:if test="${not empty requestScope.product}">
        <img width="300px" src="/assets/img/products/${requestScope.product.mainPicture}"/><br>
    </c:if>
    <c:if test="${empty requestScope.product}">
        <input type="file" name="picture" accept="image/jpeg,image/png,image/gif"><br>
    </c:if>
    <fmt:message key="form.submit" var="submit"/>

    <h3><fmt:message key="form.price"/></h3>
    <input name="price" type="number" min="0" step="1" value="${requestScope.product.price}"><br>

    <input type="submit" value="${submit}">
</form>
<%@ include file="/WEB-INF/includes/footer.jsp" %>