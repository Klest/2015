<%@include file="/WEB-INF/includes/header.jsp" %>
<c:if test="${requestScope.picture}">
    <img src="/assets/img/products/${requestScope.picture}"><br>
</c:if>
<form method="post" enctype="multipart/form-data">
    <input type="file" name="picture" accept="image/jpeg,image/png,image/gif"><br>
    <fmt:message key="action.save" var="submit"/>
    <input type="submit" value="${submit}">
</form>
<%@include file="/WEB-INF/includes/footer.jsp" %>