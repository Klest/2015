<%@include file="/WEB-INF/includes/header.jsp" %>

<c:forEach items="${requestScope.subcategories}" var="subcategory">
    <p>
        <b>${subcategory.title}</b>
        [ <a href="/admin/subcategory?id=${subcategory.id}"><fmt:message key="action.edit"/></a> ]
        [ <a href="/admin/subcategory?action=del&id=${subcategory.id}"><fmt:message key="action.delete"/></a> ]
        [ <a href="/admin/picture?action=change&id=${subcategory.picture.id}"><fmt:message key="action.edit"/> <fmt:message key="plainText.pictures"/></a> ]
    </p>
</c:forEach>

<%@include file="/WEB-INF/includes/footer.jsp" %>