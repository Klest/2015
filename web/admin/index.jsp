<%@include file="/WEB-INF/includes/header.jsp" %>

<p><a href="admin/product"><fmt:message key="action.add.product"/></a></p>

<c:if test="${requestScope.isProvider}">
    <p><a href="admin/myProducts"><fmt:message key="plainText.myProducts"/></a></p>
</c:if>
<c:if test="${requestScope.isAdmin}">
    <p><a href="admin/subcategory"><fmt:message key="action.add.subcategory"/></a></p>
    <p><a href="admin/allSubcategories"><fmt:message key="plainText.subcategories"/></a></p>
    <p><a href="admin/orders"><fmt:message key="plainText.orders"/></a>
        <c:if test="${not empty requestScope.cnt}">
            (<b style="color: green">+ ${requestScope.cnt}</b>)
        </c:if>
    </p>
    <p><a href="admin/users"><fmt:message key="plainText.users"/></a></p>
</c:if>

<%@include file="/WEB-INF/includes/footer.jsp" %>