<%@ include file="/WEB-INF/includes/header.jsp" %>
<c:forEach items="${requestScope.carts}" var="cart">
    <table border="1">
        <tr>
            <td><fmt:message key="plainText.owner"/></td>
            <td><a href="/admin/users?id=${cart.owner.id}">${cart.owner.attributes.name}</a></td>
        </tr>
        <tr>
            <td><fmt:message key="plainText.creationTime"/></td>
            <td>${cart.creationTime}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.price"/></td>
            <td><fmt:formatNumber value="${cart.price}"  minFractionDigits="2"  maxFractionDigits="2"/></td>
        </tr>
        <c:forEach items="${cart.products}" var="entry">
            <tr>
                <td>${entry.key.title}</td>
                <td>${entry.value}</td>
            </tr>
        </c:forEach>
        <tr>
            <td><a href="?id=${cart.id}"><fmt:message key="action.complete"/></a></td>
        </tr>
    </table>
</c:forEach>
<%@ include file="/WEB-INF/includes/footer.jsp" %>