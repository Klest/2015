<%@include file="/WEB-INF/includes/header.jsp" %>
<c:if test="${empty requestScope.user}">
    <c:forEach items="${requestScope.users}" var="user">
        <a href="/admin/users?id=${user.id}">${user.attributes.login}</a> (${user.registrationTime})<br>
    </c:forEach>
</c:if>
<c:if test="${not empty requestScope.user}">
    <table border="1">
        <tr>
            <td>ID</td>
            <td>${requestScope.user.id}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.login"/></td>
            <td>${requestScope.user.attributes.login}</td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>${requestScope.user.attributes.email}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.name"/></td>
            <td>${requestScope.user.attributes.name}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.surname"/></td>
            <td>${requestScope.user.attributes.surname}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.mobile"/></td>
            <td>${requestScope.user.attributes.mobile}</td>
        </tr>
        <tr>
            <td><fmt:message key="form.address"/></td>
            <td>${requestScope.user.attributes.address}</td>
        </tr>
        <tr>
            <td><fmt:message key="plainText.lastVisit"/></td>
            <td>${requestScope.user.lastVisit}</td>
        </tr>
        <c:if test="${user.rights==0}">
            <tr>
                <td><a href="?id=${user.id}&action=setProvider"><fmt:message key="action.addToProviders"/></a></td>
            </tr>
        </c:if>
    </table>
</c:if>
<%@include file="/WEB-INF/includes/footer.jsp" %>