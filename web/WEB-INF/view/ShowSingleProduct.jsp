<%@include file="/WEB-INF/includes/header.jsp" %>

<c:if test="${empty requestScope.product}">
    <fmt:message key="plainText.notFound"/>
</c:if>
<c:if test="${not empty requestScope.product}">

    <div class="row">
        <script>
            function deyish(url) {
                document.getElementById("mainimage").src = url;
            }
            $(document).ready(function () {
                $('.materialboxed').materialbox();
            });
        </script>

        <div class="col-md-6" style="margin:10px 0;">

            <img width="100%" id="mainimage" class="materialboxed" src="/assets/img/products/${requestScope.product.mainPicture}"/>

            <div style="margin:10px 0;">
                <c:forEach items="${requestScope.product.pictures}" var="picture">
                        <span>
                            <img height="100px" style="cursor:pointer;" onclick="deyish('/assets/img/products/${picture}')" src="/assets/img/products/${picture}">
                            <c:if test="${requestScope.isAdmin || requestScope.product.ownerId==sessionScope.user.id}">
                                [ <a href="/admin/picture?action=remove&id=${picture.id}"><fmt:message key="action.delete"/></a> ]
                                [ <a href="/admin/picture?action=change&id=${picture.id}"><fmt:message key="action.edit"/></a> ]
                            </c:if>
                        </span>
                </c:forEach>
            </div>


        </div>

        <div class="col-md-6" style="margin:10px 0;">
            <h1 style="margin:0 0 20px;">${requestScope.product.title}</h1>
                ${requestScope.product.description}
            <div style="margin:20px 0;">
                <h1 class="ui header" style="font-family:inherit;">
                    <img src="/assets/img/manat.gif">
                    <div class="content" style="font-size:25px;">
                        <fmt:message key="form.price"/> :
                        <fmt:formatNumber value="${requestScope.product.price}" maxFractionDigits="2" minFractionDigits="2"/>
                    </div>
                </h1>


                <h1 class="ui header" style="font-family:inherit;">
                    <img src="/assets/img/box.png">
                    <div class="content" style="font-size:25px;">
                        <fmt:message key="plainText.freeShipping"/>
                    </div>
                </h1>
                <c:if test="${requestScope.isLogged}">
                    <div><a class="ui labeled icon purple button" href="/cart?action=add&id=${requestScope.product.id}" style="font-size:20px; background-color:#8A3A7A;"><i class="add to cart icon"></i><fmt:message key="action.add.toCart"/></a></div>
                </c:if>

                <c:if test="${!requestScope.isLogged}">
                    <div>
                        <a href="/login" class="ui labeled icon purple button" style="font-size:20px; margin:5px; background-color:#8A3A7A;"><i class="sign in icon"></i><fmt:message key="plainText.sign_in"/></a>
                        <a href="/signup" class="ui labeled icon purple button" style="font-size:20px; margin:5px; background-color:#8A3A7A;"><i class="add user icon"></i><fmt:message key="plainText.sign_up"/></a>


                    </div>
                </c:if>
            </div>
        </div>

    </div>


    <c:if test="${requestScope.isAdmin || requestScope.product.ownerId==sessionScope.user.id}">
        <div><a href="/admin/product?id=${requestScope.product.id}"><fmt:message key="action.edit"/></a></div>
        <div><a href="/admin/product?action=del&id=${requestScope.product.id}"><fmt:message key="action.delete"/></a></div>
        <div><a href="/admin/picture?action=add&id=${requestScope.product.id}"><fmt:message key="action.add.picture"/></a></div>
    </c:if>
</c:if>
<%@include file="/WEB-INF/includes/footer.jsp" %>