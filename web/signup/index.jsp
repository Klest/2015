<%@ include file="/WEB-INF/includes/header.jsp" %>
    
<h2 class="ui horizontal divider header">
  <i class="add user icon" style="color: #713364"></i>
  Registration
</h2>
 <div class='col-sm-6'>
    <form id="regForm" method="POST" action="/signup" onsubmit="return ajax();">
        <table class="forma">
            <tr>
                <td colspan='2'><div id="login_msg"></div></td>
            </tr>
            <tr>
                <td><fmt:message key="form.login"/></td>
                <td><input name="login" class="form-control" onchange="check('login',this.value)" required></td>
            
            </tr>

            <tr>
                <td colspan='2'><div id="password_msg"></div></td>
            </tr>

            <tr>
                <td><fmt:message key="form.password"/></td>
                <td><input type="password" name="password" class="form-control" onchange="check('password',this.value)" required></td>
            
            </tr>


            <tr>
                <td colspan='2'><div id="password2_msg"></div></td>
            </tr>


            <tr>
                <td><fmt:message key="form.again"/></td>
                <td><input type="password" name="password2" class="form-control" onchange="checkPassword2()" required></td>
            
            </tr>

            <tr>
                <td colspan='2'><div id="email_msg"></div></td>
            </tr>

            <tr>
                <td>E-mail</td>
                <td><input type="email" name="email" class="form-control" onchange="check('email',this.value)" required></td>
            
            </tr>

            <tr>
                <td colspan='2'><div id="name_msg"></div></td>
            </tr>


            <tr>
                <td><fmt:message key="form.name"/></td>
                <td><input name="name" class="form-control" onchange="check('name',this.value)"></td>
            
            </tr>

            <tr>
                <td colspan='2'><div id="surname_msg"></div></td>
            </tr>

            <tr>
                <td><fmt:message key="form.surname"/></td>
                <td><input name="surname" class="form-control" onchange="check('surname',this.value)"></td>
            
            </tr>

        
            <tr>
                <td colspan='2'><div id="mobile_msg"></div></td>
            </tr>

            <tr>
                <td><fmt:message key="form.mobile"/></td>
                <td><input type="tel" name="mobile" class="form-control" onchange="check('mobile',this.value)"></td>
            
            </tr>

            <tr>
                <td colspan='2'><div id="address_msg"></div></td>
            </tr>


            <tr>
                <td><fmt:message key="form.address"/></td>
                <td><input name="address" class="form-control" onchange="check('address',this.value)"></td>
            
            </tr>

            <tr>
            
                <td colspan='2'><fmt:message key="form.submit" scope="page" var="submit"/><input type="submit" value="${submit}" class="registerbutton"></td>
            </tr>
        </table>
    </form>


 </div>



<%--
<h2>Registration</h2>

<form id="regForm" method="POST" action="/signup" onsubmit="return ajax();">
    <table>
        <tr>
            <td><fmt:message key="form.login"/></td>
            <td><input title="login" required></td>
        </tr>
        <tr>
            <td><fmt:message key="form.password"/></td>
            <td><input type="password" title="password" required></td>
        </tr>
        <tr>
            <td><fmt:message key="form.again"/></td>
            <td><input type="password" title="password_again" required></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><input type="email" title="email" required></td>
        </tr>
        <tr>
            <td><fmt:message key="form.name"/></td>
            <td><input title="title"></td>
        </tr>
        <tr>
            <td><fmt:message key="form.surname"/></td>
            <td><input title="surname"></td>
        </tr>
        <tr>
            <td><fmt:message key="form.mobile"/></td>
            <td><input type="tel" title="mobile"></td>
        </tr>
        <tr>
            <td><fmt:message key="form.address"/></td>
            <td><input title="address"></td>
        </tr>
        <tr>
            <fmt:message key="form.submit" scope="page" var="submit"/>
            <td><input type="submit" value="${submit}"></td>
        </tr>
    </table>
</form>
--%>
<%@ include file="/WEB-INF/includes/footer.jsp" %>