package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

@WebServlet(name = "Authenticator", urlPatterns = { "/login", "/login/" })
public class Authenticator extends HttpServlet{

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	request.getRequestDispatcher( ApplicationConstants.JSP_LOGIN ).forward( request, response );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	RequestUtil requestUtil = new RequestUtil( request );
	final String login = requestUtil.getParameter( RequestUtil.PARAM_LOGIN );
	final String password = requestUtil.getParameter( RequestUtil.PARAM_PASSWORD );
	final Pattern loginPattern = Pattern.compile( UserValidator.PATTERNS.get( User.ATTR_LOGIN ) );
	final Pattern passwordPattern = Pattern.compile( UserValidator.PATTERNS.get( User.ATTR_PASSWORD ) );
	boolean logged = false;
	User user = null;
	if( loginPattern.matcher( login ).matches( ) && passwordPattern.matcher( password ).matches( ) ){
		user = new UserHelper( ).getUser( login );
		if( user.getId( ) != -1 && user.attributes.get( User.ATTR_PASSWORD ).equals( password ) ) logged = true;
	}
	if( logged ){
		new SessionUtil( request ).setUser( user );
		response.sendRedirect( "/" );
	}else{
		Interpreter.getInterpreter( request ).putMessageKey( "error.authenticationFailed" );
		requestUtil.setMessagesToPrint( );
		doGet( request, response );
	}
}
}