package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.CartHelper;
import az.gift.server.domains.Cart;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "GovernmentViewer", urlPatterns = { "/gov", "/gov/" })
public class GovernmentViewer extends HttpServlet{
static final String GOVERNMENT_PASSWORD = "xxxxxxxxxxxxx";
static final String PARAM_KEY = "key";

// overridden
@Override
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	final String password = request.getParameter( PARAM_KEY );
	if( password != null && password.equals( GOVERNMENT_PASSWORD ) ){
		List<Cart> orders = new CartHelper( ).getCompletedCarts( );
		orders.forEach( cart -> Interpreter.getInterpreter( request ).translateProducts( cart.products.keySet( ) ) );
		request.setAttribute( ApplicationConstants.ATTR_COMPLETED_ORDERS, orders );
		request.getRequestDispatcher( ApplicationConstants.JSP_GOV ).forward( request, response );
	}else response.sendError( 404 );
}
}
