package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserRegistrar", urlPatterns = { "/signup", "/signup/" })
public class UserRegistrar extends HttpServlet{
private static final String VALID_RESPONSE_TO_AJAX = "true";

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	request.setAttribute( ApplicationConstants.ATTR_IS_REGISTRATION_PAGE, true );
	request.getRequestDispatcher( ApplicationConstants.JSP_SIGN_UP ).forward( request, response );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	RequestUtil requestUtil = new RequestUtil( request );
	UserValidator validator = new UserValidator( request );
	if( requestUtil.isAjax( ) ){
		final String responseText;
		if( validator.checkField( requestUtil.getParameter( RequestUtil.PARAM_VALIDATE ) ) ) responseText = VALID_RESPONSE_TO_AJAX;
		else responseText = Interpreter.getInterpreter( request ).getMessages( ).get( 0 );
		requestUtil.setMessagesToPrint();
		response.getWriter( ).print( responseText );
	}else if( validator.checkAllFields( ) ){
		UserHelper helper = new UserHelper( );
		User user = helper.saveUser( validator.getValidUser( ) );
		new SessionUtil( request ).setUser( user );
		Interpreter.getInterpreter( request ).putMessageKey( "msg.thanksForRegistration" );
		response.sendRedirect( "/" );
	}else{
		requestUtil.setMessagesToPrint( );
		doGet( request, response );
	}
}
}