package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.SubcategoryHelper;
import az.gift.server.domains.Subcategory;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.ProductValidator;
import az.gift.server.validators.SubcategoryValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SubcategoryController", urlPatterns = { "/admin/subcategory", "/admin/subcategory/" })
@MultipartConfig(location = ApplicationConstants.PICTURE_UPLOAD_DIR, fileSizeThreshold = ApplicationConstants.FILE_UPLOAD_THRESHOLD, maxFileSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB, maxRequestSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB + 1)
public class SubcategoryController extends HttpServlet{
public static final String ACTION_DELETE = "del";

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	User user = new SessionUtil( request ).getUser( );
	if( user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ){
		final RequestUtil util = new RequestUtil( request );
		final String action = util.getParameter( RequestUtil.PARAM_ACTION );
		final Interpreter interpreter = Interpreter.getInterpreter( request );
		final SubcategoryHelper helper = new SubcategoryHelper( );
		Subcategory chosen = new RequestUtil( request ).getSubcategory( );
		if( action.equals( ACTION_DELETE ) ){
			if( chosen.getId( ) != -1 ){
				helper.delete( chosen );
				interpreter.putMessageKey( "msg.subcategoryDeleted" );
				interpreter.updateInterpreter( util );
			}else interpreter.putMessageKey( "msg.subcategoryNotFound" );
			response.sendRedirect( "/" );
		}else{
			if( chosen.getId( ) != -1 ) request.setAttribute( ApplicationConstants.ATTR_SUBCATEGORY, chosen );
			request.getRequestDispatcher( ApplicationConstants.JSP_ADMIN_SUBCATEGORY ).forward( request, response );
		}
	}else response.sendError( 403 );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	User user = new SessionUtil( request ).getUser( );
	if( user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ){
		final RequestUtil util = new RequestUtil( request );
		final Interpreter interpreter = Interpreter.getInterpreter( request );
		final SubcategoryValidator validator = new SubcategoryValidator( request );
		final SubcategoryHelper helper = new SubcategoryHelper( );
		Subcategory chosen = util.getSubcategory( );
		if( validator.checkAllFields( chosen ) ){
			Subcategory validatedSubcategory = validator.getValidSubcategory( );
			if( chosen.getId( ) == -1 ){
				helper.saveSubcategory( validatedSubcategory );
				validator.savePicture( );
			}else helper.updateSubcategory( validatedSubcategory );
			interpreter.putMessageKey( "msg.saved" );
			interpreter.updateInterpreter( util );
			response.sendRedirect( "/" );
		}else{
			util.setMessagesToPrint( );
			request.getRequestDispatcher( ApplicationConstants.JSP_ADMIN_SUBCATEGORY ).forward( request, response );
		}
	}else response.sendError( 403 );
}
}
