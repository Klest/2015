package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserPanel", urlPatterns = { "/panel", "/panel/" })
public class UserPanel extends HttpServlet{
// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	final String change = new RequestUtil( request ).getParameter( RequestUtil.PARAM_CHANGE );
	if( UserValidator.isPossibleToChange( change ) ) request.setAttribute( ApplicationConstants.ATTR_FORM, change );
	request.getRequestDispatcher( ApplicationConstants.JSP_PANEL ).forward( request, response );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	RequestUtil requestUtil = new RequestUtil( request );
	final String change = requestUtil.getParameter( RequestUtil.PARAM_CHANGE );
	UserValidator validator = new UserValidator( request );
	if( UserValidator.isPossibleToChange( change ) && validator.checkField( change ) ){
		UserHelper helper = new UserHelper( );
		User user = new SessionUtil( request ).getUser( );
		final String validatedValue = validator.getValidUser( ).attributes.get( change );
		user.attributes.put( change, validatedValue );
		SessionUtil sessionUtil = new SessionUtil( request );
		sessionUtil.setUser( user );
		helper.updateUser( user, change );
		Interpreter.getInterpreter( request ).putMessageKey( "msg.saved" );
		response.sendRedirect( "/" );
	}else{
		requestUtil.setMessagesToPrint( );
		doGet( request, response );
	}
}
}