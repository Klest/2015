package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.domains.Product;
import az.gift.server.utils.SessionUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowUsers", urlPatterns = { "/admin/myProducts", "/admin/myProducts/" })
public class MyProductViewer extends HttpServlet{

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	List<Product> products = new ProductHelper( ).getMyProducts( new SessionUtil( request ).getUser( ) );
	Interpreter.getInterpreter( request ).translateProducts( products );
	request.setAttribute( ApplicationConstants.ATTR_PRODUCTS, products );
	request.getRequestDispatcher( ApplicationConstants.JSP_MY_PRODUCTS ).forward( request, response );
}
}