package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.ProductPaging;
import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.database.DAO.SubcategoryHelper;
import az.gift.server.domains.Product;
import az.gift.server.domains.Subcategory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "HomeViewer", urlPatterns = { "" })
public class HomeViewer extends HttpServlet{
public static final int NUMBER_OF_MOST_POPULAR_TO_VIEW = 10;

void fillSubcategories( HttpServletRequest request ){
	SubcategoryHelper helper = new SubcategoryHelper( );
	List<Subcategory> subcategories = new ArrayList<>( 5 );
	subcategories.add( helper.getSubcategory( 6 ) );
	subcategories.add( helper.getSubcategory( 2 ) );
	subcategories.add( helper.getSubcategory( 5 ) );
	subcategories.add( helper.getSubcategory( 3 ) );
	subcategories.add( helper.getSubcategory( 4 ) );
	Interpreter.getInterpreter( request ).translateSubcategories( subcategories );
	request.setAttribute( ApplicationConstants.ATTR_HOME_PAGE_BIJUTERIA, subcategories.get( 0 ) );
	request.setAttribute( ApplicationConstants.ATTR_HOME_PAGE_WOOD_CARVING, subcategories.get( 1 ) );
	request.setAttribute( ApplicationConstants.ATTR_HOME_PAGE_HOUSE_WARMING, subcategories.get( 2 ) );
	request.setAttribute( ApplicationConstants.ATTR_HOME_PAGE_PAINTING, subcategories.get( 3 ) );
	request.setAttribute( ApplicationConstants.ATTR_HOME_PAGE_ANNIVERSARY, subcategories.get( 4 ) );
}

// overridden
@Override
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	fillSubcategories( request );
	List<Product> products = new ProductHelper( ).getMostPopularProducts( new ProductPaging( NUMBER_OF_MOST_POPULAR_TO_VIEW, 1 ) );
	Interpreter.getInterpreter( request ).translateProducts( products );
	request.setAttribute( ApplicationConstants.ATTR_PRODUCTS, products );
	request.getRequestDispatcher( ApplicationConstants.JSP_ROOT ).forward( request, response );
}
}
