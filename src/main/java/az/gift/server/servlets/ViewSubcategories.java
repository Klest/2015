package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.utils.RequestUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ViewSubcategories", urlPatterns = { "/admin/allSubcategories","/admin/allSubcategories/" })
public class ViewSubcategories extends HttpServlet{

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	new RequestUtil( request ).setAllSubcategoriesToRequest( );
	request.getRequestDispatcher( ApplicationConstants.JSP_ALL_SUBCATEGORIES ).forward( request, response );
}
}
