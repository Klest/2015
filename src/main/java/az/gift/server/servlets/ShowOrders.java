package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.CartHelper;
import az.gift.server.domains.Cart;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@WebServlet(name = "ShowOrders", urlPatterns = { "/admin/orders" })
public class ShowOrders extends HttpServlet{
static AtomicLong newOrdersCounter = new AtomicLong( 0 );

static void increaseCnt( ){ newOrdersCounter.incrementAndGet( ); }

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	User user = new SessionUtil( request ).getUser( );
	if( user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ){
		newOrdersCounter.set( 0 );
		CartHelper helper = new CartHelper( );
		Cart chosen = new RequestUtil( request ).getCart( );
		Interpreter interpreter = Interpreter.getInterpreter( request );
		if( chosen.getId( ) != -1 ){
			helper.setCompletedStatus( chosen );
			interpreter.putMessageKey( "msg.saved" );
			response.sendRedirect( "/" );
		}else{
			List<Cart> activeCarts = helper.getActiveCarts( );
			interpreter.translateCarts( activeCarts );
			request.setAttribute( ApplicationConstants.ATTR_ACTIVE_CARTS, activeCarts );
			request.getRequestDispatcher( ApplicationConstants.JSP_ORDERS ).forward( request, response );
		}
	}else response.sendError( 403 );
}
}
