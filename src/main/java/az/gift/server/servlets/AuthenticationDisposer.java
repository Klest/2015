package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AuthenticationDisposer", urlPatterns = { "/exit", "/exit/" })
public class AuthenticationDisposer extends HttpServlet{
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	request.getSession( ).removeAttribute( ApplicationConstants.ATTR_USER );
	response.sendRedirect( "/" );
}
}