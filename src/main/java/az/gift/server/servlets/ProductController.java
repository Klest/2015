package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.database.DAO.SubcategoryHelper;
import az.gift.server.domains.Product;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.ProductValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;

@WebServlet(name = "ProductController", urlPatterns = { "/admin/product", "/admin/product/" })
@MultipartConfig(location = ApplicationConstants.PICTURE_UPLOAD_DIR, fileSizeThreshold = ApplicationConstants.FILE_UPLOAD_THRESHOLD, maxFileSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB, maxRequestSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB + 1)

public class ProductController extends HttpServlet{
public static final String ACTION_DELETE = "del";

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	RequestUtil requestUtil = new RequestUtil( request );
	User user = new SessionUtil( request ).getUser( );
	Product chosen = requestUtil.getProduct( );
	final Interpreter interpreter = Interpreter.getInterpreter( request );
	final String action = requestUtil.getParameter( RequestUtil.PARAM_ACTION );
	if( chosen.getId( ) == -1 || chosen.getOwnerId( ) == user.getId( ) || user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ) if( action.equals( ACTION_DELETE ) ){
		if( chosen.getId( ) == -1 ) interpreter.putMessageKey( "error.productNotFound" );
		else{
			new ProductHelper( ).deactivate( chosen );
			interpreter.putMessageKey( "msg.productDeleted" );
		}
		response.sendRedirect( "/" );
	}else{
		if( chosen.getId( ) != -1 ){
			new SubcategoryHelper( ).getAssignedSubcategories( chosen );
			request.setAttribute( ApplicationConstants.ATTR_PRODUCT, chosen );
			request.setAttribute( ApplicationConstants.ATTR_CHECKED_SUBCATEGORIES, new HashSet<>( chosen.getSubcategories( ) ) );
		}
		requestUtil.setAllSubcategoriesToRequest( );
		request.getRequestDispatcher( ApplicationConstants.JSP_ADMIN_PRODUCT ).forward( request, response );
	}
	else response.sendError( 403 );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	RequestUtil requestUtil = new RequestUtil( request );
	Product chosen = requestUtil.getProduct( );
	User user = new SessionUtil( request ).getUser( );
	if( chosen.getId( ) == -1 || chosen.getOwnerId( ) == user.getId( ) || user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ){
		ProductValidator validator = new ProductValidator( request );
		if( validator.checkAllFields( chosen ) ){
			ProductHelper helper = new ProductHelper( );
			if( chosen.getId( ) == -1 ){
				validator.getValidProduct( ).setOwnerId( user.getId( ) );
				helper.saveProduct( validator.getValidProduct( ) );
				validator.savePictureToHDD( );
			}else helper.updateProduct( validator.getValidProduct( ) );
			Interpreter.getInterpreter( request ).putMessageKey( "msg.saved" );
			response.sendRedirect( "/" );
		}else{
			requestUtil.setMessagesToPrint( );
			doGet( request, response );
		}
	}else response.sendError( 403 );
}
}
