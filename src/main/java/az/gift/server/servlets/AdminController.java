package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AdminController", urlPatterns = { "/admin/", "/admin" })
public class AdminController extends HttpServlet{
// overridden
@Override
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	long cnt = ShowOrders.newOrdersCounter.get( );
	if( cnt > 0 ) request.setAttribute( ApplicationConstants.ATTR_NEW_ORDERS_COUNTER, cnt );
	request.getRequestDispatcher( ApplicationConstants.JSP_ADMIN ).forward( request, response );
}
}
