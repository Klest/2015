package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.PictureHelper;
import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.domains.Picture;
import az.gift.server.domains.Product;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;
import az.gift.server.validators.PictureValidator;
import az.gift.server.validators.ProductValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PictureController", urlPatterns = { "/admin/picture", "/admin/picture/" })
@MultipartConfig(location = ApplicationConstants.PICTURE_UPLOAD_DIR, fileSizeThreshold = ApplicationConstants.FILE_UPLOAD_THRESHOLD, maxFileSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB, maxRequestSize = 1024 * 1024 * ProductValidator.MAX_PICTURES_SIZE_MB + 1)
public class PictureController extends HttpServlet{
public static final String ACTION_ADD_PICTURE = "add";
public static final String ACTION_DELETE_PICTURE = "remove";
public static final String ACTION_REPLACE_PICTURE = "change";

// overridden
@Override
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	final RequestUtil util = new RequestUtil( request );
	final String action = util.getParameter( RequestUtil.PARAM_ACTION );
	if( action.equals( ACTION_DELETE_PICTURE ) ){
		final Interpreter interpreter = Interpreter.getInterpreter( request );
		Picture toDelete = util.getPicture( );
		if( toDelete.getId( ) == -1 ) interpreter.putMessageKey( "error.pictureNotFound" );
		else{
			PictureHelper helper = new PictureHelper( );
			if( helper.isMainPicture( toDelete ) ) interpreter.putMessageKey( "error.deleteMainPic" );
			else{
				helper.deletePicture( toDelete );
				interpreter.putMessageKey( "msg.pictureDeleted" );
			}
		}
		response.sendRedirect( "/" );
	}else request.getRequestDispatcher( ApplicationConstants.JSP_PICTURE ).forward( request, response );
}

@Override
protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	final RequestUtil util = new RequestUtil( request );
	final String action = util.getParameter( RequestUtil.PARAM_ACTION );
	boolean accessDenied = false;
	User user = new SessionUtil( request ).getUser( );
	if( user.getRights( ) == ApplicationConstants.PROVIDER_PERMISSION_CODE ) if( action.equals( ACTION_ADD_PICTURE ) ) accessDenied = util.getProduct( ).getOwnerId( ) != user.getId( );
	else{
		Picture picture = util.getPicture( );
		Product withThisPicture = new ProductHelper( ).getProduct( picture );
		if( withThisPicture.getOwnerId( ) != user.getId( ) ) accessDenied = true;
	}

	if( accessDenied ) response.sendError( 403 );
	else{
		final PictureValidator validator = new PictureValidator( request );
		final Interpreter interpreter = Interpreter.getInterpreter( request );
		if( action.equals( ACTION_ADD_PICTURE ) ){
			Product toAssign = util.getProduct( );
			if( toAssign.getId( ) != -1 ) if( validator.checkPicture( ) ){
				Picture picture = validator.getPicture( );
				final PictureHelper helper = new PictureHelper( );
				helper.savePicture( picture );
				validator.savePicture( );
				toAssign.setMainPicture( picture );
				helper.assignPictureToProduct( toAssign );
				interpreter.putMessageKey( "msg.saved" );
			}else interpreter.putMessageKey( "error.incorrectFormat.picture" );
			else interpreter.putMessageKey( "error.productNotFound" );
		}else if( action.equals( ACTION_REPLACE_PICTURE ) ){
			Picture existed = util.getPicture( );
			if( existed.getId( ) != -1 ) if( validator.checkPicture( ) ){
				Picture picture = validator.getPicture( );
				picture.setId( existed.getId( ) );
				validator.savePicture( );
				new PictureHelper( ).savePicturesExtension( picture );
				interpreter.putMessageKey( "msg.saved" );
			}else interpreter.putMessageKey( "error.incorrectFormat.picture" );
			else interpreter.putMessageKey( "error.pictureNotFound" );
		}
		response.sendRedirect( "/" );
	}
}
}