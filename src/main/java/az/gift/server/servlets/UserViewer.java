package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserViewer", urlPatterns = { "/admin/users", "/admin/users/" })
public class UserViewer extends HttpServlet{
final static String ACTION_NEW_PROVIDER = "setProvider";

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	User user = new SessionUtil( request ).getUser( );
	if( user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE ){
		RequestUtil util = new RequestUtil( request );
		User chosen = util.getUser( );
		if( chosen.getId( ) != -1 ){
			final String action = util.getParameter( RequestUtil.PARAM_ACTION );
			if( action != null && action.equals( ACTION_NEW_PROVIDER ) ){
				chosen.setRights( ApplicationConstants.PROVIDER_PERMISSION_CODE );
				new UserHelper( ).updateUserRights( chosen );
			}
			request.setAttribute( ApplicationConstants.ATTR_USER, chosen );
		}else request.setAttribute( ApplicationConstants.ATTR_USERS, new UserHelper( ).getAllUsers( ) );
		request.getRequestDispatcher( ApplicationConstants.JSP_USERS ).forward( request, response );
	}else response.sendError( 403 );
}
}
