package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.MultipleProductSelection;
import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.domains.Product;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowProductsController", urlPatterns = { "/products", "/products/" })
public class ShowProductsController extends HttpServlet{
public static final String ACTION_FIND = "find";

private static RequestDispatcher getDispatcher( HttpServletRequest request ){
	final String path;
	RequestUtil util = new RequestUtil( request );
	final String action = util.getParameter( RequestUtil.PARAM_ACTION );
	if( action.equals( ACTION_FIND ) ){
		ProductHelper helper = new ProductHelper( );
		Product wanted = helper.searchProduct( util.getParameter( RequestUtil.PARAM_PRODUCT ) );
		if( wanted.getId( ) == -1 ){
			int id = util.getParameterAsInt( RequestUtil.PARAM_PRODUCT );
			wanted = helper.getProductWithAllPictures( id );
		}
		if( wanted.getId( ) != -1 ){
			wanted.translate( Interpreter.getInterpreter( request ).language );
			request.setAttribute( ApplicationConstants.ATTR_PRODUCT, wanted );
		}
		path = ApplicationConstants.JSP_SINGLE_PRODUCT;
	}else{
		SessionUtil sessionUtil = new SessionUtil( request );
		MultipleProductSelection selection = new MultipleProductSelection( request );
		selection.setPaging( sessionUtil.getProductPaging( util ) );
		selection.setSorting( sessionUtil.getProductSorting( util ) );
		if( selection.getCategory( ).getId( ) != -1 ) request.setAttribute( ApplicationConstants.ATTR_CATEGORY, selection.getCategory( ) );
		List<Product> selected = new ProductHelper( ).getSelectedProducts( selection );
		Interpreter.getInterpreter( request ).translateProducts( selected );
		request.setAttribute( ApplicationConstants.ATTR_PRODUCTS, selected );
		path = ApplicationConstants.JSP_PRODUCTS;
	}
	return request.getRequestDispatcher( path );
}

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	getDispatcher( request ).forward( request, response );
}

protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	getDispatcher( request ).forward( request, response );
}
}
