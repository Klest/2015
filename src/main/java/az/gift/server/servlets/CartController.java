package az.gift.server.servlets;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.domains.Cart;
import az.gift.server.domains.Product;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CartController", urlPatterns = { "/cart", "/cart/" })
public class CartController extends HttpServlet{
public static final String ACTION_ADD = "add";
public static final String ACTION_REMOVE = "remove";
public static final String ACTION_CONFIRM = "confirm";

// overridden
protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	final String action = request.getParameter( RequestUtil.PARAM_ACTION );
	SessionUtil util = new SessionUtil( request );
	RequestUtil requestUtil = new RequestUtil( request );
	Cart cart = util.getCart( );
	boolean redirectToHome = false;
	Product chosen;

	if( action != null ) if( action.equals( ACTION_ADD ) ){
		chosen = requestUtil.getProduct( );
		if( chosen.getId( ) > 0 ) cart.add( chosen );
	}else if( action.equals( ACTION_REMOVE ) ){
		chosen = requestUtil.getProduct( );
		if( chosen.getId( ) > 0 ) cart.remove( chosen );
	}else if( action.equals( ACTION_CONFIRM ) ){
		cart.confirm( );
		ShowOrders.increaseCnt();
		cart = new Cart( );
		cart.owner = util.getUser( );
		redirectToHome = true;
		Interpreter.getInterpreter( request ).putMessageKey( "msg.confirmedPurchase" );
	}
	Interpreter.getInterpreter( request ).translateProducts( cart.products.keySet( ) );
	util.setCart( cart );
	request.setAttribute( ApplicationConstants.ATTR_CART_IS_EMPTY, cart.products.size( ) == 0 );
	if( redirectToHome ) response.sendRedirect( "/" );
	else request.getRequestDispatcher( ApplicationConstants.JSP_CART ).forward( request, response );
}
}