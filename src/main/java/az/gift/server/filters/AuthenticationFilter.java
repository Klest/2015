package az.gift.server.filters;

import az.gift.server.utils.SessionUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AuthenticationFilter")
public class AuthenticationFilter implements Filter{

// overridden
public void init( FilterConfig config ) throws ServletException{

}

public void doFilter( ServletRequest req, ServletResponse resp, FilterChain chain ) throws ServletException, IOException{
	if( new SessionUtil( (HttpServletRequest) req ).isLoggedIn( ) ) chain.doFilter( req, resp );
	else ( (HttpServletResponse) resp ).sendRedirect( "/login" );
}

public void destroy( ){
}

}
