package az.gift.server.filters;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "MainFilter")
public class MainFilter implements Filter{

// overridden
public void init( FilterConfig config ) throws ServletException{

}

public void doFilter( ServletRequest req, ServletResponse resp, FilterChain chain ) throws ServletException, IOException{
	resp.setContentType( "text/html" );
	req.setCharacterEncoding( "UTF-8" );
	resp.setCharacterEncoding( "UTF-8" );
	final HttpServletRequest request = (HttpServletRequest) req;
	final SessionUtil sessionUtil = new SessionUtil( request );

	if( sessionUtil.isLoggedIn( ) ){
		User user = sessionUtil.getUser( );
		new UserHelper( ).updateLastVisit( user );
		request.setAttribute( ApplicationConstants.ATTR_IS_LOGGED, true );
		request.setAttribute( ApplicationConstants.ATTR_IS_ADMIN, user.getRights( ) == ApplicationConstants.ADMIN_PERMISSION_CODE );
		request.setAttribute( ApplicationConstants.ATTR_IS_PROVIDER, user.getRights( ) == ApplicationConstants.PROVIDER_PERMISSION_CODE );
	}
	RequestUtil requestUtil = new RequestUtil( request );
	Interpreter interpreter = Interpreter.getInterpreter( request );
	if( !requestUtil.getParameter( RequestUtil.PARAM_LANGUAGE ).isEmpty( ) ) interpreter.chooseLanguage( requestUtil );
	if( interpreter.getMessages( ).size( ) > 0 ) requestUtil.setMessagesToPrint( );
	request.setAttribute( ApplicationConstants.ATTR_CATEGORIES, interpreter.getCategories( ) );
	chain.doFilter( req, resp );
}

public void destroy( ){
}

}
