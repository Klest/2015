package az.gift.server.filters;

import az.gift.server.ApplicationConstants;
import az.gift.server.utils.SessionUtil;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AuthorizationFilter")
public class AuthorizationFilter implements Filter{

// overridden
public void init( FilterConfig config ) throws ServletException{

}

public void doFilter( ServletRequest req, ServletResponse resp, FilterChain chain ) throws ServletException, IOException{
	int rights = new SessionUtil( (HttpServletRequest) req ).getUser( ).getRights( );
	if( rights == ApplicationConstants.ADMIN_PERMISSION_CODE || rights == ApplicationConstants.PROVIDER_PERMISSION_CODE ) chain.doFilter( req, resp );
	else ( (HttpServletResponse) resp ).sendError( 404 );// :-)
}

public void destroy( ){

}

}
