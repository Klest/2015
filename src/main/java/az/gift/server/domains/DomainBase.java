package az.gift.server.domains;

public abstract class DomainBase{
protected int id = -1;
protected String titleAz;
protected String titleEn;
protected String titleRu;
protected String title;

public void translate( String language ){
	title = titleAz;
	if( language.equals( "ru" ) ) title = titleRu;
	else if( language.equals( "en" ) ) title = titleEn;
}

// getters and setters
public int getId( ){ return id; }

public void setId( int id ){ this.id = id; }

public String getTitle( ){ return title; }

public String getTitleAz( ){ return titleAz; }

public void setTitleAz( String titleAz ){ this.titleAz = titleAz; }

public String getTitleEn( ){ return titleEn; }

public void setTitleEn( String titleEn ){ this.titleEn = titleEn; }

public String getTitleRu( ){ return titleRu; }

public void setTitleRu( String titleRu ){ this.titleRu = titleRu; }

// overridden
@Override
public int hashCode( ){ return getId( ); }

@Override
public boolean equals( Object other ){
	if( other == null ) return false;
	if( other == this ) return true;
	if( !( other instanceof DomainBase ) ) return false;// Is it ok?
	return getId( ) == ( (DomainBase) other ).getId( );
}
}
