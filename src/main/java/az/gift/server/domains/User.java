package az.gift.server.domains;

import az.gift.server.validators.UserValidator;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class User{
public static final String ATTR_LOGIN = "login";
public static final String ATTR_PASSWORD = "password";
public static final String ATTR_EMAIL = "email";
public static final String ATTR_NAME = "name";
public static final String ATTR_SURNAME = "surname";
public static final String ATTR_MOBILE = "mobile";
public static final String ATTR_ADDRESS = "address";

public Map<String, String> attributes;
private Date registrationTime;
private Date lastVisit;
private int rights;
private int id;

public User( ){
	id = -1;
	attributes = new HashMap<>( UserValidator.NUMBER_OF_FIELDS );
}

// getters and setters
public Map<String, String> getAttributes( ){ return attributes; }

public String getCovertlyPassword( ){ return attributes.get( "password" ).replaceAll( ".", "*" ); }

public int getId( ){ return id; }

public void setId( int id ){ this.id = id; }

public Date getLastVisit( ){ return lastVisit; }

public void setLastVisit( Date lastVisit ){ this.lastVisit = lastVisit; }

public Date getRegistrationTime( ){ return registrationTime; }

public void setRegistrationTime( Date registrationTime ){ this.registrationTime = registrationTime; }

public int getRights( ){ return rights; }

public void setRights( int rights ){ this.rights = rights; }

// overridden
@Override
public String toString( ){ return "{user:" + getId( ) + "; rights: " + rights + "; attributes:" + attributes + "}"; }

}