package az.gift.server.domains;

public class Picture{
private String extension;
private int id = -1;

public Picture( ){}

public Picture( int id ){ this.id = id; }

// getters and setters
public String getExtension( ){ return extension; }

public void setExtension( String extension ){ this.extension = extension; }

public int getId( ){ return id; }

public void setId( int id ){ this.id = id; }

// overridden
@Override
public String toString( ){ return id + "." + extension; }
}