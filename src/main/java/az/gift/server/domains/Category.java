package az.gift.server.domains;

import java.util.ArrayList;
import java.util.List;

public class Category{
public static final int NUMBER_OF_CATEGORIES = 5;
public List<Subcategory> subcategories;
private int id = -1;
private String title;

public Category( ){ subcategories = new ArrayList<>( ); }

public Category( int id ){
	this( );
	this.id = id;
}

// getters and setters
public int getId( ){ return id; }

public void setId( int id ){ this.id = id; }

public List<Subcategory> getSubcategories( ){ return subcategories; }

public String getTitle( ){ return title; }

public void setTitle( String title ){ this.title = title; }

// overridden
@Override
public String toString( ){
	return "category: " + title + "(" + id + ")\nsubcat" + subcategories;
}
}
