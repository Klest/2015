package az.gift.server.domains;

import java.util.ArrayList;
import java.util.List;

public class Product extends DomainBase{
private double price;
private String description;
private String descriptionAz;
private String descriptionRu;
private String descriptionEn;
private List<Subcategory> subcategories;
private List<Picture> pictures;
private Picture mainPicture;
private int ownerId;

public Product( ){
	subcategories = new ArrayList<>( );
	pictures = new ArrayList<>( );
}

// getters and setters
public String getDescription( ){ return description; }

public void setDescription( String description ){ this.description = description; }

public String getDescriptionAz( ){ return descriptionAz; }

public void setDescriptionAz( String descriptionAz ){ this.descriptionAz = descriptionAz; }

public String getDescriptionEn( ){ return descriptionEn; }

public void setDescriptionEn( String descriptionEn ){ this.descriptionEn = descriptionEn; }

public String getDescriptionRu( ){ return descriptionRu; }

public void setDescriptionRu( String descriptionRu ){ this.descriptionRu = descriptionRu; }

public Picture getMainPicture( ){ return mainPicture; }

public void setMainPicture( Picture mainPicture ){ this.mainPicture = mainPicture; }

public int getOwnerId( ){ return ownerId; }

public void setOwnerId( int ownerId ){ this.ownerId = ownerId; }

public List<Picture> getPictures( ){ return pictures; }

public void setPictures( List<Picture> pictures ){ this.pictures = pictures; }

public double getPrice( ){ return price; }

public void setPrice( double price ){ this.price = price; }

public List<Subcategory> getSubcategories( ){ return subcategories; }

public void setSubcategories( List<Subcategory> subcategories ){ this.subcategories = subcategories; }

// overridden
@Override
public void translate( String language ){
	super.translate( language );
	description = descriptionAz;
	if( language.equals( "ru" ) ) description = descriptionRu;
	if( language.equals( "en" ) ) description = descriptionEn;
}

@Override
public String toString( ){ return "{id:" + getId( ) + "; titleEn:'" + getTitleEn( ) + "; price:" + getPrice( ) + "'}"; }

}