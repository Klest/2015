package az.gift.server.domains;

public class Subcategory extends DomainBase{

private int category;
private Picture picture;

// getters and setters
public int getCategoryId( ){ return category; }

public void setCategoryId( int category ){ this.category = category; }

public Picture getPicture( ){ return picture; }

public void setPicture( Picture picture ){ this.picture = picture; }

// overridden
@Override
public String toString( ){ return "{id:" + getId( ) + "; title en:'" + getTitleEn( ) + "'; category:" + category + "}"; }
}
