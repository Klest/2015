package az.gift.server.domains;

import az.gift.server.database.DAO.CartHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Cart{
public Date creationTime;
public Map<Product, Integer> products = new HashMap<>( );
public User owner;
private double price;
private int id;

public Cart( ){
	creationTime = null;
	owner = null;
	price = 0;
	id = -1;
}

public void add( Product product ){
	int amount = 1;
	if( products.containsKey( product ) ) amount += products.get( product );
	products.put( product, amount );
	price += product.getPrice( );
}

public void remove( Product product ){
	if( products.containsKey( product ) ){
		products.remove( product );
		price -= product.getPrice( );
	}
}

public void updatePrice( ){
	price = 0;
	for( Map.Entry<Product, Integer> entry : products.entrySet( ) )
		price += entry.getKey( ).getPrice( ) * entry.getValue( );
}

public void confirm( ){ new CartHelper( ).saveOrder( this ); }

// getters and setters
public Date getCreationTime( ){ return creationTime; }

public int getId( ){ return id; }

public void setId( int id ){ this.id = id; }

public User getOwner( ){ return owner; }

public double getPrice( ){ return price; }

public Map<Product, Integer> getProducts( ){ return products; }

// overridden
@Override
public String toString( ){ return "{Owner:" + owner + "; price:" + getPrice( ) + "; products:" + products + "; creation_time:" + creationTime + "}"; }
}
