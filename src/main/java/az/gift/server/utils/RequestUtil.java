package az.gift.server.utils;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.ProductPaging;
import az.gift.server.ProductSorting;
import az.gift.server.database.DAO.*;
import az.gift.server.domains.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class RequestUtil{
public static final String PARAM_ID = "id";
public static final String PARAM_ACTION = "action";
public static final String PARAM_TITLE_AZ = "title_az";
public static final String PARAM_TITLE_RU = "title_ru";
public static final String PARAM_TITLE_EN = "title_en";
public static final String PARAM_SORTING = "sorting";
public static final String PARAM_CHANGE = "change";
public static final String PARAM_LANGUAGE = "lang";
public static final String PARAM_LOGIN = "login";
public static final String PARAM_PAGE_NUMBER = "page";
public static final String PARAM_PAGE_SIZE = "pageSize";
public static final String PARAM_PASSWORD = "password";
public static final String PARAM_PASSWORD_CONFIRMATION = "password2";
public static final String PARAM_VALIDATE = "validate";
public static final String PARAM_CATEGORY = "category";
public static final String PARAM_PICTURE = "picture";
public static final String PARAM_DESCRIPTION_AZ = "description_az";
public static final String PARAM_DESCRIPTION_RU = "description_ru";
public static final String PARAM_DESCRIPTION_EN = "description_en";
public static final String PARAM_PRICE = "price";
public static final String PARAM_PRODUCT = "product";
private final static String AJAX_HEADER = "X-Requested-With";
private final static String AJAX_HEADER_VALUE = "XMLHttpRequest";
private HttpServletRequest request;

public RequestUtil( HttpServletRequest request ){ this.request = request; }

public String getParameter( String parameterName ){
	final String value = request.getParameter( parameterName );
	if( value == null ) return "";
	return value;
}

public void setMessagesToPrint( ){
	Interpreter interpreter = Interpreter.getInterpreter( request );
	List<String> messages = interpreter.getMessages( );
	if( messages.size( ) > 0 ){
		request.setAttribute( ApplicationConstants.ATTR_MESSAGES, messages );
		interpreter.clearMessages( );
	}
}

public int getParameterAsInt( String parameterName ){
	String value = getParameter( parameterName );
	int number = -1;
	try{
		number = Integer.parseInt( value );
	}catch( NumberFormatException ignored ){ }
	return number;
}

public void setAllSubcategoriesToRequest( ){
	List<Subcategory> subcategories = new SubcategoryHelper( ).getAllSubcategories( );
	Interpreter.getInterpreter( request ).translateSubcategories( subcategories );
	request.setAttribute( ApplicationConstants.ATTR_SUBCATEGORIES, subcategories );
}

public List<Category> getAllCategories( Interpreter interpreter ){
	List<Category> allCategories = new ArrayList<>( );
	for( int i = 1; i <= Category.NUMBER_OF_CATEGORIES; ++i ){
		Category category = new Category( i );
		category.setTitle( interpreter.getText( "categories." + i ) );
		allCategories.add( category );
	}
	List<Subcategory> allSubcategories = new SubcategoryHelper( ).getAllSubcategories( );
	interpreter.translateSubcategories( allSubcategories );
	for( Subcategory subcategory : allSubcategories ){
		int categoryId = subcategory.getCategoryId( );
		allCategories.get( categoryId - 1 ).subcategories.add( subcategory );
	}
	return allCategories;
}

// getters and setters
public Cart getCart( ){ return new CartHelper( ).getCart( getIdFromParameters( ) ); }

public Category getCategory( ){
	int id = getIdFromParameters( );
	Category category = new Category( );
	if( id > 0 && id <= Category.NUMBER_OF_CATEGORIES ) category.setId( id );
	return category;
}

public int getCurrentPageNumber( ){
	int page = ProductPaging.DEFAULT_PAGE_NUMBER;
	try{
		page = Integer.parseInt( getParameter( PARAM_PAGE_NUMBER ) );
	}catch( NumberFormatException ignored ){ }
	return page;
}

public int getIdFromParameters( ){
	int id = -1;
	try{
		id = Integer.parseInt( getParameter( PARAM_ID ) );
	}catch( NumberFormatException ignored ){ }
	return id;
}

public int getPageSizeFromParameters( ){
	int pageSize;
	try{
		pageSize = Integer.parseInt( getParameter( PARAM_PAGE_SIZE ) );
	}catch( NumberFormatException e ){
		pageSize = ProductPaging.DEFAULT_PAGE_SIZE;
	}
	if( pageSize < 1 ) pageSize = 1;
	else if( pageSize > ProductPaging.MAX_PAGE_SIZE ) pageSize = ProductPaging.MAX_PAGE_SIZE;
	return pageSize;
}

public Product getProduct( ){ return new ProductHelper( ).getProductWithOnlyMainPicture( getIdFromParameters( ) ); }

public Picture getPicture(){ return new PictureHelper( ).getPicture( getIdFromParameters( ) );}

public HttpServletRequest getRequest( ){ return request; }

public int getSortingCodeFromParameters( ){
	try{
		int sorting = Integer.parseInt( getParameter( PARAM_SORTING ) );
		if( sorting > 0 && sorting <= ProductSorting.MAX_SORTING_CODE ) return sorting;
	}catch( NumberFormatException ignored ){ }
	return ProductSorting.DEFAULT_SORTING_CODE;
}

public Subcategory getSubcategory( ){ return new SubcategoryHelper( ).getSubcategory( getIdFromParameters( ) ); }

public User getUser( ){ return new UserHelper( ).getUser( getIdFromParameters( ) ); }

public boolean isAjax( ){
	final String header = request.getHeader( AJAX_HEADER );
	return header != null && header.equals( AJAX_HEADER_VALUE );
}

}