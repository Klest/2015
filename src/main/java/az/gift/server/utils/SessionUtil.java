package az.gift.server.utils;

import az.gift.server.ApplicationConstants;
import az.gift.server.Interpreter;
import az.gift.server.ProductPaging;
import az.gift.server.ProductSorting;
import az.gift.server.domains.Cart;
import az.gift.server.domains.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtil{
private HttpSession session;

public SessionUtil( HttpServletRequest request ){ this.session = request.getSession( ); }

public SessionUtil( HttpSession session ){ this.session = session; }

public ProductPaging getProductPaging( RequestUtil util ){
	ProductPaging paging = (ProductPaging) session.getAttribute( ApplicationConstants.ATTR_PAGING );
	if( paging == null || !util.getParameter( RequestUtil.PARAM_PAGE_SIZE ).isEmpty( ) || !util.getParameter( RequestUtil.PARAM_PAGE_NUMBER ).isEmpty( ) ){
		paging = new ProductPaging( util.getPageSizeFromParameters( ), util.getCurrentPageNumber( ) );
		session.setAttribute( ApplicationConstants.ATTR_PAGING, paging );
	}
	return paging;
}

public ProductSorting getProductSorting( RequestUtil util ){
	ProductSorting sorting = (ProductSorting) session.getAttribute( ApplicationConstants.ATTR_PRODUCT_SORTING );
	if( sorting == null || !util.getParameter( RequestUtil.PARAM_SORTING ).isEmpty( ) ){
		sorting = new ProductSorting( util );
		session.setAttribute( ApplicationConstants.ATTR_PRODUCT_SORTING, sorting );
	}
	return sorting;
}

// getters and setters
public Cart getCart( ){
	Cart cart = (Cart) session.getAttribute( ApplicationConstants.ATTR_CART );
	if( cart == null ){
		cart = new Cart( );
		cart.owner = getUser( );
	}
	return cart;
}

public void setCart( Cart cart ){ session.setAttribute( ApplicationConstants.ATTR_CART, cart ); }

public User getUser( ){
	User user = (User) session.getAttribute( ApplicationConstants.ATTR_USER );
	if( user == null ) user = new User( );
	return user;
}

public void setUser( User user ){ session.setAttribute( ApplicationConstants.ATTR_USER, user ); }

public boolean isLoggedIn( ){ return session.getAttribute( ApplicationConstants.ATTR_USER ) != null; }

public void setInterpreter( Interpreter interpreter ){ session.setAttribute( ApplicationConstants.ATTR_INTERPRETER, interpreter );}
}