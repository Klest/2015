package az.gift.server;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

@WebListener()
public class OnlineCounter implements HttpSessionListener {
	public static AtomicLong onlineUsersCounter = new AtomicLong( );

	public void sessionCreated( HttpSessionEvent se ) {
		onlineUsersCounter.incrementAndGet( );
	}

	public void sessionDestroyed( HttpSessionEvent se ) {
		onlineUsersCounter.decrementAndGet( );
	}
}
