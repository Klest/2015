package az.gift.server;

public interface ApplicationConstants{
int ADMIN_PERMISSION_CODE = 10;
int PROVIDER_PERMISSION_CODE = 5;
int FILE_UPLOAD_THRESHOLD = 1024 * 1024;
String JDBC_URI = "java:comp/env/jdbc/giftdb";
String RESOURCE_BUNDLE_PACKAGE = "az.gift.server.bundles.text";

// move to server config
String PICTURE_UPLOAD_DIR = "S:\\web\\pictures_upload";
//String PICTURE_UPLOAD_DIR = "/opt/tomcat/upload/pictures";

String JSP_SIGN_UP = "/signup/index.jsp";
String JSP_LOGIN = "/login/index.jsp";
String JSP_PANEL = "/panel/index.jsp";
String JSP_ADMIN_SUBCATEGORY = "/admin/subcategory/index.jsp";
String JSP_ADMIN_PRODUCT = "/admin/product/index.jsp";
String JSP_ORDERS = "/admin/orders/index.jsp";
String JSP_PRODUCTS = "/products/index.jsp";
String JSP_SINGLE_PRODUCT = "/WEB-INF/view/ShowSingleProduct.jsp";
String JSP_USERS = "/admin/users/index.jsp";
String JSP_CART = "/cart/index.jsp";
String JSP_ALL_SUBCATEGORIES = "/admin/allSubcategories/index.jsp";
String JSP_PICTURE = "/admin/picture/index.jsp";
String JSP_MY_PRODUCTS = "/admin/myProducts/index.jsp";
String JSP_ROOT = "index.jsp";
String JSP_ADMIN = "/admin/index.jsp";
String JSP_GOV = "/gov/index.jsp";

String ATTR_PRODUCT = "product";
String ATTR_PRODUCTS = "products";
String ATTR_CART = "cart";
String ATTR_ACTIVE_CARTS = "carts";
String ATTR_USER = "user";
String ATTR_USERS = "users";
String ATTR_CATEGORIES = "categories";
String ATTR_SUBCATEGORIES = "subcategories";
String ATTR_SUBCATEGORY = "subcategory";
String ATTR_CHECKED_SUBCATEGORIES = "checkedSubcategories";
String ATTR_MESSAGES = "messages";
String ATTR_FORM = "form";
String ATTR_IS_LOGGED = "isLogged";
String ATTR_CART_IS_EMPTY = "cartIsEmpty";
String ATTR_IS_REGISTRATION_PAGE = "isRegistrationPage";
String ATTR_INTERPRETER = "interpreter";
String ATTR_IS_ADMIN = "isAdmin";
String ATTR_IS_PROVIDER = "isProvider";
String ATTR_CATEGORY = "category";
String ATTR_PAGING = "paging";
String ATTR_PRODUCT_SORTING = "sorting";
String ATTR_NEW_ORDERS_COUNTER = "cnt";
String ATTR_COMPLETED_ORDERS = "completedOrders";
String ATTR_HOME_PAGE_BIJUTERIA = "homePageBijuteria";
String ATTR_HOME_PAGE_WOOD_CARVING = "homePageWoodCarving";
String ATTR_HOME_PAGE_HOUSE_WARMING = "homePageHouseWarming";
String ATTR_HOME_PAGE_PAINTING = "homePagePainting";
String ATTR_HOME_PAGE_ANNIVERSARY = "homePageAnniversary";
}