package az.gift.server.database.DAO;

import az.gift.server.domains.Cart;
import az.gift.server.domains.Product;
import az.gift.server.domains.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartHelper extends Helper{
private static final String ID = "id";
private static final String USER = "user";
private static final String TIME = "time";
private static final String AMOUNT = "amount";

public void saveOrder( Cart cart ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO orders VALUES(NULL,?,NULL,'active')", Statement.RETURN_GENERATED_KEYS );
		statement.setInt( 1, cart.getOwner( ).getId( ) );
		statement.executeUpdate( );
		ResultSet generatedKeys = statement.getGeneratedKeys( );
		generatedKeys.next( );
		cart.setId( generatedKeys.getInt( 1 ) );
		saveProducts( cart );
	}catch( SQLException e ){
		System.out.println( "Can't save the cart: " + cart );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void saveProducts( Cart cart ) throws SQLException{
	PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO products_in_order VALUES(?,?,?)" );
	for( Map.Entry<Product, Integer> entry : cart.products.entrySet( ) ){
		statement.setInt( 1, cart.getId( ) );
		statement.setInt( 2, entry.getKey( ).getId( ) );
		statement.setInt( 3, entry.getValue( ) );
		statement.addBatch( );
	}
	statement.executeBatch( );
}

private Map<Product, Integer> getProductsForCart( int cartsId ) throws SQLException{
	Map<Product, Integer> productsInOrder = new HashMap<>( );
	PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT p.*, pio.amount,pic.ext ext FROM products_in_order pio JOIN products p ON p.id=pio.product JOIN pictures pic ON p.main_picture_id=pic.id WHERE order_id=?" );
	statement.setInt( 1, cartsId );
	ResultSet rs = statement.executeQuery( );
	while( rs.next( ) ) productsInOrder.put( ProductHelper.createProductFromResultSet( rs ), rs.getInt( AMOUNT ) );
	return productsInOrder;
}

private Cart createCartFromResultSet( ResultSet rs ) throws SQLException{
	User owner = new UserHelper( ).getUser( rs.getInt( USER ) );
	Cart cart = new Cart( );
	cart.owner = owner;
	cart.setId( rs.getInt( ID ) );
	cart.creationTime = rs.getTimestamp( TIME );
	cart.products = getProductsForCart( cart.getId( ) );//optimization here
	cart.updatePrice( );
	return cart;
}

public Cart getCart( int id ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM orders WHERE id=? AND status='active'" );
		statement.setInt( 1, id );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createCartFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get active carts" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new Cart( );
}

public void increaseBoughtTimeForProducts( Cart cart ) throws SQLException{
	PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE products SET bought_times=bought_times+1 WHERE id IN (SELECT product FROM products_in_order WHERE order_id=?)" );
	statement.setInt( 1, cart.getId( ) );
	statement.executeUpdate( );
}

// getters and setters
public List<Cart> getActiveCarts( ){
	List<Cart> activeCarts = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM orders WHERE status='active' ORDER BY time DESC" );
		ResultSet rs = statement.executeQuery( );
		while( rs.next( ) ) activeCarts.add( createCartFromResultSet( rs ) );
	}catch( SQLException e ){
		System.out.println( "Can't get active carts" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return activeCarts;
}

public List<Cart> getCompletedCarts(){
	List<Cart> completedCarts = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM orders WHERE status='completed' ORDER BY time DESC" );
		ResultSet rs = statement.executeQuery( );
		while( rs.next( ) ) completedCarts.add( createCartFromResultSet( rs ) );
	}catch( SQLException e ){
		System.out.println( "Can't get completed carts" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return completedCarts;
}

public void setCompletedStatus( Cart cart ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE orders SET time=now(),status='completed' WHERE id=?" );
		statement.setInt( 1, cart.getId( ) );
		statement.executeUpdate( );
		increaseBoughtTimeForProducts( cart );
	}catch( SQLException e ){
		System.out.println( "Can't update status for: " + cart );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}
}