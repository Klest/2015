package az.gift.server.database.DAO;

import az.gift.server.domains.Picture;
import az.gift.server.domains.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PictureHelper extends Helper{
public static final String ID = "id";
public static final String EXTENSION = "ext";


private static Picture createPictureFromResultSet( ResultSet resultSet ) throws SQLException{
	Picture picture = new Picture( );
	picture.setId( resultSet.getInt( 1 ) );
	picture.setExtension( resultSet.getString( 2 ) );
	return picture;
}

public Picture savePicture( Picture pic ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO pictures VALUE (NULL ,?)", Statement.RETURN_GENERATED_KEYS );
		statement.setString( 1, pic.getExtension( ) );
		statement.executeUpdate( );
		ResultSet generatedKeys = statement.getGeneratedKeys( );
		generatedKeys.next( );
		pic.setId( generatedKeys.getInt( 1 ) );
	}catch( SQLException e ){
		System.out.println( "Can't save a picture" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return pic;
}

public Picture getPicture( int id ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM pictures WHERE id=?" );
		statement.setInt( 1, id );
		ResultSet resultSet = statement.executeQuery( );
		if( resultSet.next( ) ) return createPictureFromResultSet( resultSet );
	}catch( SQLException e ){
		System.out.println( "Can't get a picture" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new Picture( );
}

public void assignPictureToProduct( Product product ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO pictures_of_products VALUE (?,?)" );
		statement.setInt( 1, product.getId( ) );
		statement.setInt( 2, product.getMainPicture( ).getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "Can't assign picture to product " + product );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public boolean isMainPicture( Picture picture ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM products WHERE main_picture_id=?" );
		statement.setInt( 1, picture.getId( ) );
		ResultSet resultSet = statement.executeQuery( );
		return resultSet.next( );
	}catch( SQLException e ){
		System.out.println( "Can't get a picture" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return false;
}

public List<Picture> getPicturesAssignedToProduct( Product product ){
	List<Picture> pictures = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT p.* FROM pictures p JOIN pictures_of_products pop ON p.id=pop.picture WHERE pop.product=?" );
		statement.setInt( 1, product.getId( ) );
		ResultSet resultSet = statement.executeQuery( );
		while( resultSet.next( ) ) pictures.add( createPictureFromResultSet( resultSet ) );
	}catch( SQLException e ){
		System.out.println( "Can't get list of pictures for " + product );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return pictures;
}

public void deletePicture( Picture toDelete ){
	try{
		PreparedStatement delFromPic = database.getConnection( ).prepareStatement( "DELETE FROM pictures WHERE id=?" );
		PreparedStatement delFromPOP = database.getConnection( ).prepareStatement( "DELETE FROM pictures_of_products WHERE picture=?" );
		delFromPic.setInt( 1, toDelete.getId( ) );
		delFromPOP.setInt( 1, toDelete.getId( ) );
		delFromPic.executeUpdate( );
		delFromPOP.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "can't delete a pic: " + toDelete );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void savePicturesExtension( Picture picture ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE pictures SET ext=? WHERE id=?" );
		statement.setString( 1, picture.getExtension( ) );
		statement.setInt( 2, picture.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "Can't save picture's extension" + picture );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}
}
