package az.gift.server.database.DAO;

import az.gift.server.domains.Picture;
import az.gift.server.domains.Product;
import az.gift.server.domains.Subcategory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubcategoryHelper extends Helper{
private static final String ID = "id";
private static final String TITLE_AZ = "title_az";
private static final String TITLE_RU = "title_ru";
private static final String TITLE_EN = "title_en";
private static final String CATEGORY = "category";
private static final String PICTURE_ID = "picture_id";

private static void setValuesIntoPreparedStatement( PreparedStatement statement, Subcategory from ) throws SQLException{
	statement.setInt( 1, from.getCategoryId( ) );
	statement.setString( 2, from.getTitleAz( ) );
	statement.setString( 3, from.getTitleRu( ) );
	statement.setString( 4, from.getTitleEn( ) );
	Picture pic = from.getPicture( );
	if( pic.getId( ) == -1 ) new PictureHelper( ).savePicture( pic );
	statement.setInt( 5, pic.getId( ) );
}

private static Subcategory createSubcategoryFromResultSet( ResultSet rs ) throws SQLException{
	Subcategory subcategory = new Subcategory( );
	subcategory.setId( rs.getInt( ID ) );
	subcategory.setTitleAz( rs.getString( TITLE_AZ ) );
	subcategory.setTitleRu( rs.getString( TITLE_RU ) );
	subcategory.setTitleEn( rs.getString( TITLE_EN ) );
	subcategory.setCategoryId( rs.getInt( CATEGORY ) );
	Picture picture = new PictureHelper( ).getPicture( rs.getInt( PICTURE_ID ) );
	subcategory.setPicture( picture );
	return subcategory;
}

public static boolean existSubcategory( int id ){
	Subcategory wanted = new SubcategoryHelper( ).getSubcategory( id );
	return wanted.getId( ) != -1;
}

public static boolean existSubcategory( String title ){
	Subcategory wanted = new SubcategoryHelper( ).getSubcategory( title );
	return wanted.getId( ) != -1;
}

public void saveSubcategory( Subcategory subcategory ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO subcategories VALUES(NULL,?,?,?,?,?)" );
		setValuesIntoPreparedStatement( statement, subcategory );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "Can't save the subcategory" + subcategory );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public Subcategory getSubcategory( String title ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM subcategories WHERE title_az=? OR title_ru=? OR title_en=?" );
		statement.setString( 1, title );
		statement.setString( 2, title );
		statement.setString( 3, title );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createSubcategoryFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get the subcategory" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new Subcategory( );
}

public Subcategory getSubcategory( int id ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM subcategories WHERE id=?" );
		statement.setInt( 1, id );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createSubcategoryFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get the subcategory" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new Subcategory( );
}

public List<Subcategory> getAssignedSubcategories( Product product ){
	List<Subcategory> assignedSubcategories = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT s.* FROM subcategories s JOIN products_assigned_to_subcategories pas ON s.id = pas.subcategory WHERE pas.product = ?" );
		statement.setInt( 1, product.getId( ) );
		ResultSet rs = statement.executeQuery( );
		while( rs.next( ) ) assignedSubcategories.add( createSubcategoryFromResultSet( rs ) );
	}catch( SQLException e ){
		System.out.println( "Can't get assigned subcategories for : " + product );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	product.setSubcategories( assignedSubcategories );
	return assignedSubcategories;
}

public void updateSubcategory( Subcategory subcategory ){
	try{
		String sql = "UPDATE subcategories SET category=?,title_az=?,title_ru=?,title_en=? WHERE id=?";
		PreparedStatement statement = database.getConnection( ).prepareStatement( sql );
		setValuesIntoPreparedStatement( statement, subcategory );
		statement.setInt( 5, subcategory.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "can't update a subcategory" + subcategory );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void delete( Subcategory subcategory ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "DELETE FROM subcategories WHERE id=?" );
		statement.setInt( 1, subcategory.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "can't delete subcategory: " + subcategory );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

// getters and setters
public List<Subcategory> getAllSubcategories( ){
	List<Subcategory> list = new ArrayList<>( );
	try{
		PreparedStatement statement;
		statement = database.getConnection( ).prepareStatement( "SELECT * FROM subcategories ORDER BY id" );
		ResultSet rs = statement.executeQuery( );
		while( rs.next( ) ) list.add( createSubcategoryFromResultSet( rs ) );
	}catch( SQLException e ){
		System.out.println( "can't get all subcategories" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return list;
}
}