package az.gift.server.database.DAO;

import az.gift.server.MultipleProductSelection;
import az.gift.server.ProductPaging;
import az.gift.server.database.Database;
import az.gift.server.domains.Picture;
import az.gift.server.domains.Product;
import az.gift.server.domains.Subcategory;
import az.gift.server.domains.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductHelper extends Helper{
public static final String ID = "id";
public static final String TITLE_AZ = "title_az";
public static final String TITLE_RU = "title_ru";
public static final String TITLE_EN = "title_en";
public static final String DESCRIPTION_AZ = "description_az";
public static final String DESCRIPTION_RU = "description_ru";
public static final String DESCRIPTION_EN = "description_en";
public static final String OWNER = "owner";
public static final String PRICE = "price";
public static final String MAIN_PICTURE_ID = "main_picture_id";
public static final String BOUGHT_TIMES = "bought_times";
public static final String VIEWED_TIMES = "viewed_times";
public static final String PICTURE_EXTENSION = "ext";

public ProductHelper( ){ super( new Database( ) ); }

private static void setValuesToPreparedStatement( PreparedStatement statement, Product from ) throws SQLException{
	statement.setString( 1, from.getTitleAz( ) );
	statement.setString( 2, from.getTitleRu( ) );
	statement.setString( 3, from.getTitleEn( ) );
	statement.setString( 4, from.getDescriptionAz( ) );
	statement.setString( 5, from.getDescriptionRu( ) );
	statement.setString( 6, from.getDescriptionEn( ) );
	statement.setDouble( 7, from.getOwnerId( ) );
	statement.setDouble( 8, from.getPrice( ) );
	Picture picture = from.getMainPicture( );
	if( picture.getId( ) == -1 ) new PictureHelper( ).savePicture( picture );
	statement.setInt( 9, picture.getId( ) );
}

public static Product createProductFromResultSet( ResultSet rs ) throws SQLException{
	Product product = new Product( );
	product.setId( rs.getInt( ID ) );
	product.setTitleAz( rs.getString( TITLE_AZ ) );
	product.setTitleRu( rs.getString( TITLE_RU ) );
	product.setTitleEn( rs.getString( TITLE_EN ) );
	product.setDescriptionAz( rs.getString( DESCRIPTION_AZ ) );
	product.setDescriptionRu( rs.getString( DESCRIPTION_RU ) );
	product.setDescriptionEn( rs.getString( DESCRIPTION_EN ) );
	product.setOwnerId( rs.getInt( OWNER ) );
	product.setPrice( rs.getDouble( PRICE ) );
	Picture picture = new Picture( rs.getInt( MAIN_PICTURE_ID ) );
	picture.setExtension( rs.getString( PICTURE_EXTENSION ) );
	product.setMainPicture( picture );
	return product;
}

public static boolean existProduct( int id ){
	Product wanted = new ProductHelper( ).getProductWithOnlyMainPicture( id );
	return wanted.getId( ) != -1;
}

public static boolean existProduct( String title ){
	Product wanted = new ProductHelper( ).getProductWithOnlyMainPicture( title );
	return wanted.getId( ) != -1;
}

//return Product?
public int saveProduct( Product product ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO products VALUES(NULL,?,?,?,?,?,?,?,?,?,0,0,now(),'active')", Statement.RETURN_GENERATED_KEYS );
		setValuesToPreparedStatement( statement, product );
		statement.executeUpdate( );
		ResultSet generatedKeys = statement.getGeneratedKeys( );
		generatedKeys.next( );
		product.setId( generatedKeys.getInt( 1 ) );
		database.getConnection( ).commit( );
		saveProductSubcategories( product );
		new PictureHelper( ).assignPictureToProduct( product );
		return product.getId( );
	}catch( SQLException e ){
		System.out.println( "Can't save the product " + product.getId( ) );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return -1;
}

public void deactivate( Product product ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE products SET status='deleted' WHERE id=?" );
		statement.setInt( 1, product.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "can't delete the product: " + product );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

private void saveProductSubcategories( Product product ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO products_assigned_to_subcategories VALUES(?,?)" );
		List<Subcategory> productSubcategories = product.getSubcategories( );
		for( Subcategory subcategory : productSubcategories ){
			statement.setInt( 1, product.getId( ) );
			statement.setInt( 2, subcategory.getId( ) );
			statement.addBatch( );
		}
		statement.executeBatch( );
	}catch( SQLException e ){
		System.out.println( "Can't save product's list " + product.getId( ) );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void assignPictureToProduct( Product product, Picture pic ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO products VALUES(NULL,?,?,?,?,?,?,?,?,?,0,0,now(),'active')" );
		setValuesToPreparedStatement( statement, product );
		statement.executeUpdate( );
		database.getConnection( ).commit( );
		saveProductSubcategories( product );
	}catch( SQLException e ){
		System.out.println( "Can't assign pic to product " + product );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public Product getProductWithOnlyMainPicture( String title ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT p.*, pic.ext ext FROM products p JOIN pictures pic ON p.main_picture_id=pic.id WHERE (title_az=? OR title_ru=? OR title_en=?) AND status='active'" );
		statement.setString( 1, title );
		statement.setString( 2, title );
		statement.setString( 3, title );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createProductFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get the product by title: " + title );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new Product( );
}

public Product getProductWithOnlyMainPicture( int id ){
	Product product = new Product( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT p.*, pic.ext ext FROM products p JOIN pictures pic ON p.main_picture_id=pic.id WHERE p.id=? AND p.status='active'" );
		statement.setInt( 1, id );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ){
			product = createProductFromResultSet( rs );
			product.setSubcategories( new SubcategoryHelper( ).getAssignedSubcategories( product ) );
		}
	}catch( SQLException e ){
		System.out.println( "Can't get the product by id " + id );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return product;
}

public Product getProductWithAllPictures( int id ){
	Product product = getProductWithOnlyMainPicture( id );
	product.setPictures( new PictureHelper( ).getPicturesAssignedToProduct( product ) );
	return product;
}

public Product searchProduct( String title ){
	Product product = new ProductHelper( ).getProductWithOnlyMainPicture( title );
	product.setPictures( new PictureHelper( ).getPicturesAssignedToProduct( product ) );
	return product;
}

public List<Product> getSelectedProducts( MultipleProductSelection selection ){
	List<Product> products = new ArrayList<Product>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( selection.getSQL( ) );
		selection.setValuesToPreparedStatement( statement );
		ResultSet rs = statement.executeQuery( );
		while( rs.next( ) ) products.add( createProductFromResultSet( rs ) );
	}catch( SQLException e ){
		System.out.println( "Can't get products assigned to " + selection );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return products;
}

public List<Product> getMostPopularProducts( ProductPaging p ){
	List<Product> mostPopular = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM products p JOIN pictures pic ON p.main_picture_id=pic.id WHERE p.status='active' ORDER BY (p.bought_times+p.viewed_times) DESC " + p.getPagingPartOfSQL( ) );
		p.setPageValuesIntoPreparedStatement( statement, 0 );
		ResultSet resultSet = statement.executeQuery( );
		while( resultSet.next( ) ) mostPopular.add( createProductFromResultSet( resultSet ) );
	}catch( SQLException e ){
		System.out.println( "can't get the most popular" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return mostPopular;
}

public void updateProduct( Product product ){
	final String sqlUpdate = "UPDATE products SET title_az=?, title_ru=?, title_en=?, description_az=?, description_ru=?, description_en=?, owner=?, price=?, main_picture_id=? WHERE id=?";
	final String sqlDelSubs = "DELETE FROM products_assigned_to_subcategories WHERE product=?";
	try{
		PreparedStatement update = database.getConnection( ).prepareStatement( sqlUpdate );
		PreparedStatement delSubs = database.getConnection( ).prepareStatement( sqlDelSubs );
		setValuesToPreparedStatement( update, product );
		update.setInt( 10, product.getId( ) );
		delSubs.setInt( 1, product.getId( ) );
		update.executeUpdate( );
		delSubs.executeUpdate( );
		saveProductSubcategories( product );
	}catch( SQLException e ){
		System.out.println( "Can't update the product " + product.getId( ) );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public List<Product> getMyProducts( User owner ){
	List<Product> myProducts = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM products p JOIN pictures pic ON p.main_picture_id=pic.id WHERE p.status='active' AND p.owner=? ORDER BY p.bought_times DESC " );
		statement.setInt( 1, owner.getId( ) );
		ResultSet resultSet = statement.executeQuery( );
		while( resultSet.next( ) ){
			Product product = createProductFromResultSet( resultSet );
			product.setPictures( new PictureHelper( ).getPicturesAssignedToProduct( product ) );
			myProducts.add( product );
		}
	}catch( SQLException e ){
		System.out.println( "can't get my products" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return myProducts;
}

public Product getProduct( Picture pic ){
	Product res = null;
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM products p JOIN pictures_of_products pop ON p.id=pop.product WHERE pop.picture=?" );
		statement.setInt( 1, pic.getId( ) );
		ResultSet resultSet = statement.executeQuery( );
		if( resultSet.next( ) ) createProductFromResultSet( resultSet );
	}catch( SQLException e ){
		System.out.println( "can't get product by pic:" + pic );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return res;
}
}