package az.gift.server.database.DAO;

import az.gift.server.database.Database;

public abstract class Helper{
protected Database database;

public Helper( ){ this( new Database( ) ); }

public Helper( Database db ){ database = db; }

}