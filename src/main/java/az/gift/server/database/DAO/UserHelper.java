package az.gift.server.database.DAO;

import az.gift.server.domains.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserHelper extends Helper{

private static final String ID = "id";
private static final String LOGIN = "login";
private static final String PASSWORD = "password";
private static final String EMAIL = "email";
private static final String NAME = "name";
private static final String SURNAME = "surname";
private static final String MOBILE = "mobile";
private static final String ADDRESS = "address";
private static final String REGISTRATION_TIME = "registration_time";
private static final String LAST_VISIT = "last_visit";
private static final String RIGHTS = "rights";

private static void setValuesIntoPreparedStatement( PreparedStatement statement, User from ) throws SQLException{
	statement.setString( 1, from.attributes.get( LOGIN ) );
	statement.setString( 2, from.attributes.get( PASSWORD ) );
	statement.setString( 3, from.attributes.get( EMAIL ) );
	statement.setString( 4, from.attributes.get( NAME ) );
	statement.setString( 5, from.attributes.get( SURNAME ) );
	statement.setString( 6, from.attributes.get( MOBILE ) );
	statement.setString( 7, from.attributes.get( ADDRESS ) );
}

private static User createUserFromResultSet( ResultSet resultSet ) throws SQLException{
	User user = new User( );
	user.setId( resultSet.getInt( ID ) );
	user.attributes.put( LOGIN, resultSet.getString( LOGIN ) );
	user.attributes.put( PASSWORD, resultSet.getString( PASSWORD ) );
	user.attributes.put( EMAIL, resultSet.getString( EMAIL ) );
	user.attributes.put( NAME, resultSet.getString( NAME ) );
	user.attributes.put( SURNAME, resultSet.getString( SURNAME ) );
	user.attributes.put( MOBILE, resultSet.getString( MOBILE ) );
	user.attributes.put( ADDRESS, resultSet.getString( ADDRESS ) );
	user.setRegistrationTime( new Date( resultSet.getTimestamp( REGISTRATION_TIME ).getTime( ) ) );
	user.setLastVisit( new Date( resultSet.getTimestamp( LAST_VISIT ).getTime( ) ) );
	user.setRights( resultSet.getInt( RIGHTS ) );
	return user;
}

public static boolean userExist( int id ){
	User wanted = new UserHelper( ).getUser( id );
	return wanted.getId( ) != -1;
}

public static boolean userExist( String login ){
	User wanted = new UserHelper( ).getUser( login );
	return wanted.getId( ) != -1;
}

public User getUser( String login ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM users WHERE login=? AND status='active'" );
		statement.setString( 1, login );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createUserFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get the user: " + login );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new User( );
}

public User getUser( int id ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM users WHERE id=? AND status='active'" );
		statement.setInt( 1, id );
		ResultSet rs = statement.executeQuery( );
		if( rs.next( ) ) return createUserFromResultSet( rs );
	}catch( SQLException e ){
		System.out.println( "Can't get the user: " + id );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return new User( );
}

public void updateUser( User user, String field ){
	if( !user.attributes.containsKey( field ) ) return;
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE users SET " + field + "=? WHERE id=?" );
		statement.setString( 1, user.attributes.get( field ) );
		statement.setInt( 2, user.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "Can't update a user " + user + " \n Field: " + field );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void updateUserRights( User user ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE users SET rights=? WHERE id=?" );
		statement.setInt( 1, user.getRights( ) );
		statement.setInt( 2, user.getId( ) );
		statement.executeUpdate( );
	}catch( SQLException e ){
		System.out.println( "Can't update user's rights: " + user );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public void updateLastVisit( User user ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "UPDATE users SET last_visit=now() WHERE id=?" );
		statement.setInt( 1, user.getId( ) );
		statement.executeUpdate( );
		user.setLastVisit( new Date( ) );
	}catch( SQLException e ){
		System.out.println( "Can't update last visit for user " + user );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
}

public User saveUser( User user ){
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "INSERT INTO users VALUES(NULL,?,?,?,?,?,?,?,now(),now(),0,'active')", Statement.RETURN_GENERATED_KEYS );
		setValuesIntoPreparedStatement( statement, user );
		statement.executeUpdate( );
		ResultSet generatedKeys = statement.getGeneratedKeys( );
		generatedKeys.next( );
		user.setId( generatedKeys.getInt( 1 ) );
	}catch( SQLException e ){
		System.out.println( "Can't save user: " + user );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return user;
}

// getters and setters
public List<User> getAllUsers( ){
	List<User> users = new ArrayList<>( );
	try{
		PreparedStatement statement = database.getConnection( ).prepareStatement( "SELECT * FROM users WHERE status='active' ORDER BY registration_time DESC" );
		ResultSet resultSet = statement.executeQuery( );
		while( resultSet.next( ) ) users.add( createUserFromResultSet( resultSet ) );
	}catch( SQLException e ){
		System.out.println( "Can't get list of users" );
		e.printStackTrace( );
	}finally{
		database.close( );
	}
	return users;
}
}