package az.gift.server;

import az.gift.server.utils.RequestUtil;

public class ProductSorting{
public static final int MAX_SORTING_CODE = 6;
public static final int DEFAULT_SORTING_CODE = 5;
private int sortingCode;
private String language;
public ProductSorting( RequestUtil util ){
	sortingCode = util.getSortingCodeFromParameters( );
	language = Interpreter.getInterpreter( util.getRequest( ) ).language;
}

// getters and setters
public int getSortingCode( ){ return sortingCode; }

public String getSortingPartOfSql( ){
	final String orderBy;
	switch( sortingCode ){
		case 1:
			orderBy = "price";
			break;
		case 2:
			orderBy = "price desc";
			break;
		case 3:
			orderBy = "title_" + language;
			break;
		case 4:
			orderBy = "title_" + language + " desc";
			break;
		case 5:
			orderBy = "(viewed_times + bought_times) desc";
			break;
		case 6:
			orderBy = "creation_time desc";
			break;
		default:
			orderBy = "";
	}
	return "ORDER BY " + orderBy;
}

// overridden
@Override
public String toString( ){
	return "ProductSorting{sortingCode=" + sortingCode + ", language='" + language + '}';
}
}
