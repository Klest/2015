package az.gift.server.validators;

import az.gift.server.database.DAO.ProductHelper;
import az.gift.server.database.DAO.SubcategoryHelper;
import az.gift.server.domains.Product;
import az.gift.server.domains.Subcategory;
import az.gift.server.utils.RequestUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class ProductValidator extends Validator{
public static final String SUBCATEGORY = "subcategory";

public static final int MAX_PICTURES_SIZE_MB = 50;
public static final int MAX_TITLE_LENGTH = 255;
public static final int MIN_TITLE_LENGTH = 3;
public static final int MAX_DESCRIPTION_LENGTH = 1000;
private static final int MAX_PRICE = 9999;

private PictureValidator pictValidator;
private Product validProduct;

public ProductValidator( HttpServletRequest request ){ super( request ); }

public static int extractSubcategoryId( String parameter ){
	int id = -1;
	String suffix = parameter.substring( "subcategory_".length( ) );
	try{
		id = Integer.parseInt( suffix );
	}catch( NumberFormatException ignored ){ }
	return id;
}

private boolean checkTitle( String field ){
	final String title = getXssSafeStringFromRequest( field );
	final boolean flag;
	if( title.length( ) > MAX_TITLE_LENGTH || title.length( ) < MIN_TITLE_LENGTH ){
		flag = false;
		putFieldIncorrectErrorToMessages( field );
	}else flag = true;
	return flag;
}

private boolean checkDescription( String field ){
	final String description = getXssSafeStringFromRequest( field );
	final boolean flag = description.length( ) <= MAX_DESCRIPTION_LENGTH;
	if( !flag ) putFieldIncorrectErrorToMessages( field );
	return flag;
}

private boolean checkPrice( ){
	final String stringPrice = getXssSafeStringFromRequest( RequestUtil.PARAM_PRICE );
	double value = -1;
	try{
		value = Double.parseDouble( stringPrice );
	}catch( NumberFormatException ignored ){ }

	final boolean flag = value >= 0 && value < MAX_PRICE;
	if( flag ) validProduct.setPrice( value );
	else putFieldIncorrectErrorToMessages( RequestUtil.PARAM_PRICE );
	return flag;
}

private boolean checkAllSubcategories( ){
	List<Subcategory> correctChosenSubcategories = new ArrayList<>( );
	SubcategoryHelper helper = new SubcategoryHelper( );
	Enumeration<String> allParameters = requestUtil.getRequest( ).getParameterNames( );
	while( allParameters.hasMoreElements( ) ){
		String parameter = allParameters.nextElement( );
		if( requestUtil.getParameter( parameter ).equals( "true" ) ){
			int id = extractSubcategoryId( parameter );
			Subcategory subcategory = helper.getSubcategory( id );
			if( subcategory.getId( ) != -1 ) correctChosenSubcategories.add( subcategory );
		}
	}
	boolean flag = correctChosenSubcategories.size( ) == 0;
	if( flag ) interpreter.putMessageKey( "error.subcategoryNotChosen" );
	else validProduct.setSubcategories( correctChosenSubcategories );
	return !flag;
}

public boolean checkAllTitlesUniqueness( ){
	boolean flag = true;
	if( !checkTitleUniqueness( validProduct.getTitleAz( ) ) ) flag = false;
	if( !checkTitleUniqueness( validProduct.getTitleRu( ) ) ) flag = false;
	if( !checkTitleUniqueness( validProduct.getTitleEn( ) ) ) flag = false;
	return flag;
}

private boolean checkTitleUniqueness( String title ){
	Product withSameTitle = new ProductHelper( ).getProductWithOnlyMainPicture( title );
	boolean flag = withSameTitle.getId( ) == validProduct.getId() || withSameTitle.getId( ) == -1;
	if( !flag ){
		final String errorMessage = MessageFormat.format( interpreter.getText( "error.itemExist" ), title );
		interpreter.addMessage( errorMessage );
	}
	return flag;
}

public boolean checkAllFields( Product toChange ) throws IOException, ServletException{
	boolean flag;
	validProduct = toChange;
	flag = checkAllFields( );
	if( toChange.getId( ) == -1 ){
		PictureValidator validator = new PictureValidator( requestUtil.getRequest( ) );
		if( !validator.checkPicture( ) ){
			flag = false;
			putFieldIncorrectErrorToMessages( RequestUtil.PARAM_PICTURE );
		}else{
			validProduct.setMainPicture( validator.getPicture( ) );
			pictValidator = validator;
		}
	}
	return flag;
}

public void savePictureToHDD( ) throws IOException{ pictValidator.savePicture( ); }

// getters and setters
public Product getValidProduct( ){ return validProduct; }

// overridden
@Override
public boolean checkAllFields( ) throws IOException, ServletException{
	boolean flag = true;
	if( !checkTitle( RequestUtil.PARAM_TITLE_AZ ) ) flag = false;
	else validProduct.setTitleAz( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_AZ ) );
	if( !checkTitle( RequestUtil.PARAM_TITLE_RU ) ) flag = false;
	else validProduct.setTitleRu( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_RU ) );
	if( !checkTitle( RequestUtil.PARAM_TITLE_EN ) ) flag = false;
	else validProduct.setTitleEn( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_EN ) );
	if( !checkDescription( RequestUtil.PARAM_DESCRIPTION_AZ ) ) flag = false;
	else validProduct.setDescriptionAz( getXssSafeStringFromRequest( RequestUtil.PARAM_DESCRIPTION_AZ ) );
	if( !checkDescription( RequestUtil.PARAM_DESCRIPTION_RU ) ) flag = false;
	else validProduct.setDescriptionRu( getXssSafeStringFromRequest( RequestUtil.PARAM_DESCRIPTION_RU ) );
	if( !checkDescription( RequestUtil.PARAM_DESCRIPTION_EN ) ) flag = false;
	else validProduct.setDescriptionEn( getXssSafeStringFromRequest( RequestUtil.PARAM_DESCRIPTION_EN ) );
	if( !checkAllTitlesUniqueness( ) ) flag = false;
	if( !checkAllSubcategories( ) ) flag = false;
	if( !checkPrice( ) ) flag = false;
	return flag;
}
}