package az.gift.server.validators;

import az.gift.server.Interpreter;
import az.gift.server.utils.RequestUtil;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class Validator{
protected Interpreter interpreter;
protected RequestUtil requestUtil;

public Validator( HttpServletRequest request ){
	requestUtil = new RequestUtil( request );
	interpreter = Interpreter.getInterpreter( request );
}

protected static String getPreparedValue( String s ){
	if( s == null || s.isEmpty( ) ) return null;
	else return s;
}

public abstract boolean checkAllFields( ) throws IOException, ServletException;

protected void putFieldIncorrectErrorToMessages( String field ){ interpreter.putMessageKey( "error.incorrectFormat." + field ); }

public String getXssSafeStringFromRequest( String field ){ return StringEscapeUtils.escapeHtml4( requestUtil.getParameter( field ).trim( ) ); }

}
