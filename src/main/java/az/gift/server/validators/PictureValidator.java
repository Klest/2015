package az.gift.server.validators;

import az.gift.server.domains.Picture;
import az.gift.server.utils.RequestUtil;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

public class PictureValidator{
private Part picturePart;
private Picture picture;
private HttpServletRequest request;

public PictureValidator( HttpServletRequest request ){
	this.request = request;
	picture = new Picture( );
}

public boolean checkPicture( ) throws IOException, ServletException{
	picturePart = request.getPart( RequestUtil.PARAM_PICTURE );
	final boolean isCorrectPicture = picturePart.getContentType( ).substring( 0, 5 ).equals( "image" );
	if( isCorrectPicture ) picture.setExtension( FilenameUtils.getExtension( picturePart.getSubmittedFileName( ) ) );
	else picturePart.delete( );
	return isCorrectPicture;
}

public void savePicture( ) throws IOException{ picturePart.write( picture.toString( ) ); }

// getters and setters
public Picture getPicture( ){ return picture; }
}
