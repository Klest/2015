package az.gift.server.validators;

import az.gift.server.database.DAO.UserHelper;
import az.gift.server.domains.User;
import az.gift.server.utils.RequestUtil;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
//todo: save only hash of passwords
public class UserValidator extends Validator{
public static final int NUMBER_OF_FIELDS = 8;
public static final Map<String, String> PATTERNS = new HashMap<>( NUMBER_OF_FIELDS );
public static final Map<String, Integer> MAX_LENGTH = new HashMap<>( );
public static final Map<String, Integer> MIN_LENGTH = new HashMap<>( );

static{
	MAX_LENGTH.put( User.ATTR_LOGIN, 30 );
	MAX_LENGTH.put( User.ATTR_PASSWORD, 30 );
	MAX_LENGTH.put( User.ATTR_EMAIL, 30 );
	MAX_LENGTH.put( User.ATTR_NAME, 50 );
	MAX_LENGTH.put( User.ATTR_SURNAME, 60 );
	MAX_LENGTH.put( User.ATTR_MOBILE, 30 );
	MAX_LENGTH.put( User.ATTR_ADDRESS, 250 );

	MIN_LENGTH.put( User.ATTR_LOGIN, 5 );
	MIN_LENGTH.put( User.ATTR_PASSWORD, 5 );
	MIN_LENGTH.put( User.ATTR_EMAIL, 0 );
	MIN_LENGTH.put( User.ATTR_NAME, 0 );
	MIN_LENGTH.put( User.ATTR_SURNAME, 0 );
	MIN_LENGTH.put( User.ATTR_MOBILE, 0 );
	MIN_LENGTH.put( User.ATTR_ADDRESS, 0 );

	PATTERNS.put( User.ATTR_LOGIN, String.format( "[a-zA-Z0-9]{%d,%d}$", MIN_LENGTH.get( User.ATTR_LOGIN ), MAX_LENGTH.get( User.ATTR_LOGIN ) ) );
	PATTERNS.put( User.ATTR_PASSWORD, String.format( "[a-zA-Z0-9]{%d,%d}$", MIN_LENGTH.get( User.ATTR_PASSWORD ), MAX_LENGTH.get( User.ATTR_PASSWORD ) ) );
	PATTERNS.put( User.ATTR_EMAIL, "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$" );
}

public User validUser;

public UserValidator( HttpServletRequest request ){
	super( request );
	validUser=new User();
}

public static boolean isPossibleToChange( String change ){ return !change.equals( User.ATTR_LOGIN ) && UserValidator.MAX_LENGTH.containsKey( change ); }

public boolean checkField( String field ){
	if( !MAX_LENGTH.containsKey( field ) ){
		interpreter.putMessageKey( "error.error" );
		return false;
	}
	final String value = getXssSafeStringFromRequest( field );
	if( field.equals( User.ATTR_LOGIN ) && !checkLogin( value ) ) return false;
	else if( field.equals( User.ATTR_PASSWORD ) && !checkPassword( ) ) return false;
	else if( !checkFieldFormat( field, value ) ){
		putFieldIncorrectErrorToMessages( field );
		return false;
	}
	validUser.attributes.put( field, getPreparedValue( value ) );
	return true;
}

protected boolean checkFieldFormat( String field, String value ){
	if( value.length( ) > MAX_LENGTH.get( field ) ) return false;
	if( value.length( ) < MIN_LENGTH.get( field ) ) return false;
	return !( PATTERNS.containsKey( field ) && !value.matches( PATTERNS.get( field ) ) );
}

private boolean checkLogin( String login ){
	if( !checkFieldFormat( User.ATTR_LOGIN, login ) ){
		String errorMessage = interpreter.getText( "error.incorrectFormat.login" );
		errorMessage = MessageFormat.format( errorMessage, MAX_LENGTH.get( User.ATTR_LOGIN ), MIN_LENGTH.get( User.ATTR_LOGIN ) );
		interpreter.addMessage( errorMessage );
		return false;
	}
	if( UserHelper.userExist( login ) ){
		interpreter.addMessage( MessageFormat.format( interpreter.getText( "error.loginIsTaken" ), login ) );
		return false;
	}
	return true;
}

private boolean checkPassword( ){
	final String password = requestUtil.getParameter( RequestUtil.PARAM_PASSWORD );
	if( !checkFieldFormat( User.ATTR_PASSWORD, password ) ){
		String errorMessage = interpreter.getText( "error.incorrectFormat.password" );
		interpreter.addMessage( MessageFormat.format( errorMessage, MAX_LENGTH.get( User.ATTR_PASSWORD ), MIN_LENGTH.get( User.ATTR_PASSWORD ) ) );
		return false;
	}
	if( !requestUtil.isAjax( ) ){
		final String passwordConfirm = requestUtil.getParameter( RequestUtil.PARAM_PASSWORD_CONFIRMATION );
		if( !passwordConfirm.equals( password ) ){
			interpreter.putMessageKey( "error.password2" );
			return false;
		}
	}
	return true;
}

// getters and setters
public User getValidUser( ){
	Date now = new Date( );
	validUser.setLastVisit( now );
	validUser.setRegistrationTime( now );
	validUser.setRights( 0 );
	return validUser;
}

// overridden
@Override
public boolean checkAllFields( ){
	boolean flag = true;
	for( String key : MAX_LENGTH.keySet( ) )
		if( !checkField( key ) ) flag = false;
	return flag;
}
}