package az.gift.server.validators;

import az.gift.server.database.DAO.SubcategoryHelper;
import az.gift.server.domains.Category;
import az.gift.server.domains.Subcategory;
import az.gift.server.utils.RequestUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.MessageFormat;

public class SubcategoryValidator extends Validator{
private static final int MAX_TITLE_LENGTH = 200;
private static final int MIN_TITLE_LENGTH = 3;
private PictureValidator picValidator;
private Subcategory validSubcategory;

public SubcategoryValidator( HttpServletRequest request ){ super( request ); }

private boolean checkTitle( String field ){
	final String title = getXssSafeStringFromRequest( field );
	final boolean flag;
	if( title.length( ) > MAX_TITLE_LENGTH || title.length( ) < MIN_TITLE_LENGTH ){
		flag = false;
		putFieldIncorrectErrorToMessages( field );
	}else flag = true;
	return flag;
}

private boolean checkCategoryId( String categoryId ){
	int id = -1;
	try{
		id = Integer.parseInt( categoryId );
	}catch( NumberFormatException ignored ){ }
	final boolean flag = id > 0 && id <= Category.NUMBER_OF_CATEGORIES;
	if( flag ) validSubcategory.setCategoryId( id );
	else putFieldIncorrectErrorToMessages( RequestUtil.PARAM_CATEGORY );
	return flag;
}

public boolean checkAllTitleUniqueness( ){
	boolean flag = true;
	if( !checkTitleUniqueness( validSubcategory.getTitleAz( ) ) ) flag = false;
	if( !checkTitleUniqueness( validSubcategory.getTitleRu( ) ) ) flag = false;
	if( !checkTitleUniqueness( validSubcategory.getTitleEn( ) ) ) flag = false;
	return flag;
}

private boolean checkTitleUniqueness( String title ){
	SubcategoryHelper helper = new SubcategoryHelper( );
	Subcategory withSameTitle = helper.getSubcategory( title );
	boolean isUnique = withSameTitle.getId( ) == validSubcategory.getId( ) || withSameTitle.getId( ) == -1;
	if( !isUnique ){
		final String errorText = MessageFormat.format( interpreter.getText( "error.subcategoryExist" ), title );
		interpreter.addMessage( errorText );
	}
	return isUnique;
}

public boolean checkAllFields( Subcategory toChange ) throws IOException, ServletException{
	boolean flag;
	validSubcategory = toChange;
	flag = checkAllFields( );
	if( toChange.getId( ) == -1 ){
		PictureValidator validator = new PictureValidator( requestUtil.getRequest( ) );
		if( !validator.checkPicture( ) ){
			flag = false;
			putFieldIncorrectErrorToMessages( RequestUtil.PARAM_PICTURE );
		}else{
			validSubcategory.setPicture( validator.getPicture( ) );
			picValidator = validator;
		}
	}
	return flag;
}

public void savePicture( ) throws IOException{ picValidator.savePicture( ); }

// getters and setters
public Subcategory getValidSubcategory( ){ return validSubcategory; }

// overridden
@Override
public boolean checkAllFields( ){
	boolean flag = true;
	if( checkTitle( RequestUtil.PARAM_TITLE_AZ ) ) validSubcategory.setTitleAz( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_AZ ) );
	else flag = false;
	if( checkTitle( RequestUtil.PARAM_TITLE_RU ) ) validSubcategory.setTitleRu( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_RU ) );
	else flag = false;
	if( checkTitle( RequestUtil.PARAM_TITLE_EN ) ) validSubcategory.setTitleEn( getXssSafeStringFromRequest( RequestUtil.PARAM_TITLE_EN ) );
	else flag = false;
	if( !checkCategoryId( requestUtil.getParameter( RequestUtil.PARAM_CATEGORY ) ) ) flag = false;
	if( !checkAllTitleUniqueness( ) ) flag = false;
	return flag;
}
}