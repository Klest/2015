package az.gift.server;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProductPaging{
public static final int DEFAULT_PAGE_SIZE = 20;
public static final int DEFAULT_PAGE_NUMBER = 1;
public static final int MAX_PAGE_NUMBER = 100;
public static final int MAX_PAGE_SIZE = 200;
private int pageSize = DEFAULT_PAGE_SIZE;
private int currentPageNumber = DEFAULT_PAGE_NUMBER;

public ProductPaging( int pageSize, int currentPage ){
	if( pageSize >= 1 && pageSize <= MAX_PAGE_SIZE ) this.pageSize = pageSize;
	if( currentPage >= 1 && currentPage < MAX_PAGE_NUMBER ) currentPageNumber = currentPage;
}

public void setPageValuesIntoPreparedStatement( PreparedStatement ps, int idx ) throws SQLException{
	final int from = pageSize * ( currentPageNumber - 1 );
	ps.setInt( ++idx, from );
	ps.setInt( ++idx, pageSize );
}

// getters and setters
public int getCurrentPageNumber( ){ return currentPageNumber; }

public int getPageSize( ){ return pageSize; }

public String getPagingPartOfSQL( ){ return "limit ?,?"; }
}
