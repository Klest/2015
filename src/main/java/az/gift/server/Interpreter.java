package az.gift.server;

import az.gift.server.domains.Cart;
import az.gift.server.domains.Category;
import az.gift.server.domains.Product;
import az.gift.server.domains.Subcategory;
import az.gift.server.utils.RequestUtil;
import az.gift.server.utils.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class Interpreter{
public static final String AZERI = "az";
public static final String RUSSIAN = "ru";
public static final String ENGLISH = "en";
public String language;
private ResourceBundle resourceBundle;
private List<String> messages;
private List<Category> categories;

public Interpreter( HttpServletRequest request ){
	this.language = AZERI;
	messages = new ArrayList<>( );
	RequestUtil util = new RequestUtil( request );
	chooseLanguage( util );
	setResourceBundle( );
	setCategories( util );
	new SessionUtil( request ).setInterpreter( this );
}

public static Interpreter getInterpreter( HttpServletRequest request ){
	Interpreter interpreter = (Interpreter) request.getSession( ).getAttribute( ApplicationConstants.ATTR_INTERPRETER );
	if( interpreter == null ) interpreter = new Interpreter( request );
	return interpreter;
}

public static boolean isValidLanguage( String language ){ return language.equals( AZERI ) || language.equals( RUSSIAN ) || language.equals( ENGLISH ); }

public void putMessageKey( String messageKey ){
	final String translatedMessage = getText( messageKey );
	addMessage( translatedMessage );
}

public void addMessage( String message ){ messages.add( message ); }

public void chooseLanguage( RequestUtil util ){
	final String language = util.getParameter( RequestUtil.PARAM_LANGUAGE );
	if( isValidLanguage( language ) && !language.equals( this.language ) ){
		this.language = language;
		setResourceBundle( );
		setCategories( util );
	}
}

public void updateInterpreter(RequestUtil util){ setCategories( util ); }

public void translateSubcategories( List<Subcategory> subcategories ){
	for( Subcategory subcategory : subcategories )
		subcategory.translate( language );
}

private void setResourceBundle( ){ resourceBundle = ResourceBundle.getBundle( ApplicationConstants.RESOURCE_BUNDLE_PACKAGE, new Locale( language ) ); }

public String getText( String messageKey ){
	try{
		return resourceBundle.getString( messageKey );
	}catch( MissingResourceException mre ){
		System.out.println( "Message not found: " + messageKey );
		mre.printStackTrace( );
	}
	return "";
}

public void clearMessages( ){ messages = new ArrayList<>( ); }

public void translateCarts( List<Cart> carts ){
	for( Cart cart : carts )
		translateProducts( cart.products.keySet( ) );
}

public void translateProducts( Collection<Product> products ){
	for( Product product : products )
		product.translate( language );
}

// getters and setters
public List<Category> getCategories( ){ return categories; }

private void setCategories( RequestUtil util ){ categories = util.getAllCategories( this ); }

public String getLanguage( ){ return language; }

public List<String> getMessages( ){ return messages; }

// overridden
@Override
public String toString( ){ return "categories: " + categories + "\nmessages: " + messages + "\nlang: " + language; }
}