package az.gift.server;

import az.gift.server.domains.Category;
import az.gift.server.domains.Subcategory;
import az.gift.server.utils.RequestUtil;

import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MultipleProductSelection{
public static final String ACTION_ALL_IN_CATEGORY = "inCategory";
public static final String ACTION_ALL_IN_SUBCATEGORY = "inSubcategory";
private ProductSorting sorting;
private Category category;
private Subcategory subcategory;
private ProductPaging paging;

public MultipleProductSelection( HttpServletRequest request ){
	RequestUtil requestUtil = new RequestUtil( request );
	category = new Category( );
	subcategory = new Subcategory( );
	final String action = requestUtil.getParameter( RequestUtil.PARAM_ACTION );

	if( action.equals( ACTION_ALL_IN_CATEGORY ) ) category = requestUtil.getCategory( );
	if( action.equals( ACTION_ALL_IN_SUBCATEGORY ) ){
		subcategory = requestUtil.getSubcategory( );
		category = Interpreter.getInterpreter( request ).getCategories( ).get( subcategory.getCategoryId( ) - 1 );
	}
}

// getters and setters
public Category getCategory( ){ return category; }

public String getSQL( ){
	return getSelectionPartOfSQL( ) + sorting.getSortingPartOfSql( ) + " " + paging.getPagingPartOfSQL( );
}

private String getSelectionPartOfSQL( ){
	if( subcategory.getId( ) != -1 ) return "SELECT p.*, pic.* " +
			"FROM products p JOIN products_assigned_to_subcategories pas ON p.id = pas.product JOIN pictures pic ON p.main_picture_id = pic.id " +
			"WHERE pas.subcategory=? and p.status='active' ";
	if( category.getId( ) != -1 ) return "SELECT p.*, pic.* " +
			"FROM products p JOIN products_assigned_to_subcategories pas ON p.id = pas.product " +
			"JOIN subcategories s ON pas.subcategory = s.id " +
			"JOIN pictures pic ON p.main_picture_id = pic.id " +
			"WHERE s.category=? and p.status='active' ";
	return "SELECT p.*, pic.* " +
			"FROM products p" +
			"JOIN pictures pic ON p.main_picture_id = pic.id" +
			"WHERE p.status='active' ";
}

public void setPaging( ProductPaging paging ){ this.paging = paging; }

public void setSorting( ProductSorting sorting ){ this.sorting = sorting; }

public void setValuesToPreparedStatement( PreparedStatement ps ) throws SQLException{
	int idx = 1;
	if( subcategory.getId( ) != -1 ) ps.setInt( 1, subcategory.getId( ) );
	else if( category.getId( ) != -1 ) ps.setInt( 1, category.getId( ) );
	else idx = 0;
	paging.setPageValuesIntoPreparedStatement( ps, idx );
}

// overridden
@Override
public String toString( ){
	return "MultipleProductSelection{sorting=" + sorting + ", category=" + category + ", subcategory=" + subcategory + ", " + '}';
}
}