CREATE TABLE users (
  id                INT UNSIGNED                         NOT NULL PRIMARY KEY AUTO_INCREMENT,
  login             VARCHAR(255)                         NOT NULL UNIQUE,
  password          VARCHAR(255)                         NOT NULL,
  email             VARCHAR(255),
  name              VARCHAR(255),
  surname           VARCHAR(255),
  mobile            VARCHAR(100),
  address           TEXT,
  last_visit        TIMESTAMP                            NOT NULL,
  registration_time TIMESTAMP                            NOT NULL,
  rights            TINYINT                              NOT NULL             DEFAULT 0,
  status            ENUM("active", "deleted", "blocked") NOT NULL             DEFAULT "active"
);

CREATE TABLE provider_info (
  id             INT UNSIGNED              NOT NULL REFERENCES users (id),
  description_az TEXT,
  description_ru TEXT,
  description_en TEXT,
  status         ENUM("active", "blocked") NOT NULL DEFAULT "active"
);

CREATE TABLE subcategories (
  id         INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  category   TINYINT      NOT NULL,
  title_az   VARCHAR(255) NOT NULL UNIQUE,
  title_ru   VARCHAR(255) NOT NULL UNIQUE,
  title_en   VARCHAR(255) NOT NULL UNIQUE,
  picture_id INT UNSIGNED NULL REFERENCES pictures (id)
);

CREATE TABLE products (
  id              INT UNSIGNED                       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title_az        VARCHAR(255)                       NOT NULL,
  title_ru        VARCHAR(255)                       NOT NULL,
  title_en        VARCHAR(255)                       NOT NULL,
  description_az  TEXT,
  description_ru  TEXT,
  description_en  TEXT,
  owner           INT UNSIGNED REFERENCES users (id) NOT NULL             DEFAULT 1,
  color           VARCHAR(255),
  material        INT UNSIGNED REFERENCES material (id),
  price           DECIMAL(8, 2)                      NOT NULL,
  main_picture_id INT UNSIGNED REFERENCES pictures (id),
  bought_times    INT UNSIGNED                       NOT NULL             DEFAULT 0,
  viewed_times    INT                                NOT NULL             DEFAULT 0,
  creation_time   TIMESTAMP                          NOT NULL             DEFAULT now(),
  votes           BIGINT UNSIGNED                    NOT NULL             DEFAULT 0,
  mark            BIGINT UNSIGNED                    NOT NULL             DEFAULT 0,
  status          ENUM("active", "deleted")          NOT NULL             DEFAULT "active"
);

CREATE TABLE material (
  id      INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name_az VARCHAR(255) NOT NULL UNIQUE,
  name_ru VARCHAR(255) NOT NULL UNIQUE,
  name_en VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE tags (
  id   INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE tags_assigned_to_product (
  tag_id     INT UNSIGNED NOT NULL REFERENCES tags (id),
  product_id INT UNSIGNED NOT NULL REFERENCES products (id)
);

CREATE TABLE products_assigned_to_subcategories (
  product     INT UNSIGNED NOT NULL REFERENCES products (id),
  subcategory INT UNSIGNED NOT NULL REFERENCES subcategories (id),
  PRIMARY KEY (product, subcategory)
);

CREATE TABLE pictures_of_products (
  product INT UNSIGNED NOT NULL REFERENCES products (id),
  picture INT UNSIGNED NOT NULL REFERENCES pictures (id),
  PRIMARY KEY (picture, product)
);

CREATE TABLE pictures (
  id  INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  ext VARCHAR(255) NOT NULL
);

CREATE TABLE orders (
  id     INT UNSIGNED                NOT NULL PRIMARY KEY AUTO_INCREMENT,
  user   INT UNSIGNED                NOT NULL REFERENCES USERS (id),
  time   TIMESTAMP                   NOT NULL             DEFAULT now(),
  status ENUM("active", "completed") NOT NULL             DEFAULT "active"
);

CREATE TABLE products_in_order (
  order_id INT UNSIGNED      NOT NULL REFERENCES orders (id),
  product  INT UNSIGNED      NOT NULL REFERENCES products (id),
  amount   SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (order_id, product)
);